<?php

require_once "Core/controllerinterface.php";
require_once "Models/modelousuario.php";
require_once "Models/modelologs.php";


/**
 * Class Login. Controlador Login, gestiona la interacción entre la vista y el modelo
 */
class Login implements ControllerInterface
{

    /**
     * Función principal del controlador. Se encarga de realizar las distintas acciones según $controllerParam
     * @param $controllerParam acción a realizar por el controlador
     * @param $requestParams información adicional, se introduce en la url.
     */
    public function run($controllerParam, $requestParams)
    {
        $modelologs = new ModeloLogs();
        switch ($controllerParam) {
            case "login":
                if($this->checkCredentials()){
                    $modelologs->addLog("Usuario ".$_POST['user']." ha iniciado sesión", 0, $_SESSION['user_id']);
                    header("Location: " . $_SERVER['HTTP_REFERER']);
                }else{
                    $modelologs->addLog("Error al iniciar sesión con usuario ".$_POST['user'], 4, null);
                    header("Location: ".Config::BASE_URL);
                }
                break;
            case "logout":
                if(isset($_SESSION['usuario'])) {
                    $modelologs->addLog("Usuario " . $_SESSION['usuario'] . " ha cerrado sesión",1, $_SESSION['user_id']);
                    Session::closeSession();
                    header("Location: " . Config::BASE_URL);
                }
                break;
        }
    }

    /**
     * Función para comprobar el tipo de usuario
     * @param $tipo string tipo de usuario
     * @return bool true Si el tipo corresponde con el tipo de usuario de la sesión false en otro caso.
     */
    public static function checkType($tipo){
        // Si ya está logueado
        if(isset($_SESSION['usuario'])){
            if($tipo === "any") {
                return true;
            }

            if($tipo === "admin"){
                return $_SESSION['user_type'] == 1;
            }

            return $_SESSION['user_type'] == 0;
        }
        return false;
    }

    /**
     * Funcion para comprobar las credenciales de el usuario.
     * Se ayuda del controlador de usuario para comprobar las credenciales.
     * @return bool true si las credenciales son correctas, false en otro caso.
     */
    private function checkCredentials(){
        $modelousuario = new ModeloUsuario();
        if(isset($_POST['user']) && isset($_POST['pass'])) {
            $userData = $modelousuario->checkCredentials($_POST['user'], $_POST['pass']);
            if ($userData != null) {
                if($userData['estado'] == 1) {
                    $_SESSION['usuario'] = $_POST['user'];
                    $_SESSION['user_type'] = $userData['tipo'];
                    $_SESSION['user_id'] = $userData['id'];
                    return true;
                }
                $_SESSION["errormsg"] = "El usuario no está activo";
            }else {
                $_SESSION["errormsg"] = "Credenciales incorrectas";
            }
        }
        return false;
    }

    /**
     * Función para obtener el tipo de usuario.
     * @return string el tipo de usuario.
     */
    public static function getUserType(){
        if(isset($_SESSION['usuario'])){
            if($_SESSION['user_type'] == 1) {
                return "admin";
            }

            return "colaborador";
        }

        return "anon";

    }
}

?>