<?php

require_once "Core/controllerinterface.php";
require_once "Views/HTMLopen.php";
require_once "Views/HTMLheader.php";
require_once "Views/HTMLnav.php";
require_once "Views/HTMLpag_index.php";
require_once "Views/HTMLpag_404.php";
require_once "Views/HTMLaside.php";
require_once "Views/HTMLfooter.php";
require_once "Views/HTMLclose.php";
require_once "Views/HTMLpag_nuevaincidencia.php";
require_once "Views/HTMLpag_editarincidencia.php";
require_once "Views/HTMLpag_gestionusuarios.php";
require_once "Views/HTMLpag_gestionbbdd.php";
require_once "Views/HTMLpag_logs.php";
require_once "Controllers/login.php";
require_once "Models/modeloincidencia.php";
require_once "Models/modelovaloracion.php";
require_once "Models/modelocomentario.php";
require_once "Models/modeloimagen.php";
require_once "Models/modelousuario.php";
require_once "Models/modelologs.php";
require_once "Views/HTMLpag_usuarioincidencias.php";


/**
 * Class Page. Clase controladora principal.
 * Se encarga de cargar las distintas partes de la página.
 */
class Page implements ControllerInterface
{

    /**
     * Función principal de el controlador. Cargará distinto contenido según $controllerParam
     * @param $controllerParam parámetro que indica la página a ser cargada.
     * @param $requestParams parámetro extra introducido en la url.
     */
    public function run($controllerParam, $requestParams)
    {

        $userType = Login::getUserType();

        HTMLopen(strtolower($controllerParam));

        HTMLheader();

        switch ($userType) {
            case "admin":
                HTMLnavAdministrador();
                break;
            case "colaborador":
                HTMLnavColaborador();
                break;
            default:
                HTMLnav();
                break;
        }


        switch ($controllerParam) {
            case "index":
                $modeloIncidencia = new ModeloIncidencia();
                $modeloComentario = new ModeloComentario();
                $modeloValoracion = new ModeloValoracion();
                $modeloImagen = new ModeloImagen();

                if (isset($_POST['textobusqueda']) || isset($_POST['lugar']) || isset($_POST['estado'])) {
                    $filtroestado = $filtroTextoBusqueda = $filtrolugar = '';
                    isset($_POST['filtro']) ? $orden = $_POST['filtro'] : $orden = 'antiguedad';
                    if (isset($_POST['textobusqueda'])) $filtroTextoBusqueda = filter_var($_POST['textobusqueda'], FILTER_SANITIZE_STRING);
                    if (isset($_POST['lugar'])) $filtrolugar = filter_var($_POST['lugar'], FILTER_SANITIZE_STRING);
                    if (isset($_POST['estado'])) $filtroestado = filter_var($_POST['estado'], FILTER_SANITIZE_STRING);

                    $todas_incidencias = $modeloIncidencia->getIncidenciasFiltradas($orden, $filtroTextoBusqueda, $filtrolugar, $filtroestado);

                } else {
                    isset($_POST['filtro']) ? $orden = $_POST['filtro'] : $orden = 'antiguedad';
                    $todas_incidencias = $modeloIncidencia->getAllIncidencias($orden);
                }

                $datos = array();
                $i = 0;

                foreach ($todas_incidencias as $incidencia) {
                    $comentarios = $modeloComentario->getComentariosIncidencia($incidencia['id']);
                    $valoraciones = $modeloValoracion->getValoracionesIncidencia($incidencia['id']);
                    $imagenes = $modeloImagen->getImagenesIncidencia($incidencia['id']);

                    $datos[$i]['incidencia'] = $incidencia;
                    $datos[$i]['comentarios'] = $comentarios;
                    $datos[$i]['valoraciones'] = $valoraciones;
                    $datos[$i]['imagenes'] = $imagenes;
                    $i++;
                }

                HTMLpag_index($datos);
                break;
            case "nuevaincidencia":
                if ($userType !== "anon") {
                    HTMLpag_nuevaincidencia();
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "editarincidencia":
                $modeloIncidencia = new ModeloIncidencia();
                if ((!ctype_digit($requestParams) || empty($requestParams)) || !$modeloIncidencia->incidenciaExists((int)$requestParams)) {
                    $_SESSION["errormsg"] = "No se ha indicado la incidencia a editar o no existe";
                    header("Location: " . Config::BASE_URL . '404');
                }
                if ($userType === "admin" || ($userType === "colaborador" && $modeloIncidencia->userIsOwner($requestParams, $_SESSION['user_id']))) {
                    $datosIncidencia = $modeloIncidencia->getIncidencia($requestParams);
                    HTMLeditar_incidencia($datosIncidencia);
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "editarusuario":
                $modelousuario = new ModeloUsuario();
                if ((!ctype_digit($requestParams) || empty($requestParams))) {
                    if (isset($_SESSION['user_id'])) {
                        /*$idusuario = $_SESSION['user_id'];
                        $datos = $modelousuario->getAllData($idusuario);

                        if ($userType === "admin") {
                            HTMLpag_gestionusuarios(true, null, true, $datos);
                        } else if ($userType === "colaborador") {
                            HTMLpag_gestionusuarios(true, null, false, $datos);
                        } else {
                            $_SESSION["errormsg"] = "Acceso denegado";
                            header("Location: " . Config::BASE_URL . '404');
                        }*/

                        header("Location: " . Config::BASE_URL . 'usuario/editar/'.$_SESSION['user_id']);
                    } else {
                        $_SESSION['errormsg'] = 'Acceso denegado';
                        header("Location: " . Config::BASE_URL . '404');
                    }
                } else {
                    if ($modelousuario->usuarioExists((int)$requestParams) && ($userType === "admin" || $requestParams === $_SESSION['user_id'])) {
                        $datos = $modelousuario->getAllData($requestParams);
                        if ($userType === "admin") {
                            HTMLpag_gestionusuarios(true, null, true, $datos);
                        } else if ($userType === "colaborador") {
                            HTMLpag_gestionusuarios(true, null, false, $datos);
                        }
                    } else {
                        $_SESSION['errormsg'] = 'Acceso denegado';
                        header("Location: " . Config::BASE_URL . '404');
                    }
                }
                break;
            case "editarivaloresincidencia":
                $modeloIncidencia = new ModeloIncidencia();
                if ($userType === "admin") {
                    $datosIncidencia = $modeloIncidencia->getIncidencia($requestParams);
                    HTMLeditar_incidencia($datosIncidencia);
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;

            case "usuarioincidencias":
                $modeloIncidencia = new ModeloIncidencia();
                $modeloComentario = new ModeloComentario();
                $modeloValoracion = new ModeloValoracion();
                $modeloImagen = new ModeloImagen();
                if ($userType !== "anon") {
                    $i = 0;

                    if ($userType === "colaborador") {
                        $incidencias = $modeloIncidencia->getIncidenciasUsuario($_SESSION['user_id']);
                    } else {

                        if(isset($_POST['formCuantasIncidencias']) && $_POST['formCuantasIncidencias']=='tusincidencias'){
                            $incidencias = $modeloIncidencia->getIncidenciasUsuario($_SESSION['user_id']);
                        }
                        else{
                            $incidencias = $modeloIncidencia->getAllIncidencias('antiguedad');
                        }
                    }

                    $datos = array();
                    foreach ($incidencias as $incidencia) {
                        $comentarios = $modeloComentario->getComentariosIncidencia($incidencia['id']);
                        $valoraciones = $modeloValoracion->getValoracionesIncidencia($incidencia['id']);
                        $imagenes = $modeloImagen->getImagenesIncidencia($incidencia['id']);

                        $datos[$i]['incidencia'] = $incidencia;
                        $datos[$i]['comentarios'] = $comentarios;
                        $datos[$i]['valoraciones'] = $valoraciones;
                        $datos[$i]['imagenes'] = $imagenes;
                        $i++;
                    }

                    $admin = false;
                    if($userType == 'admin'){
                        $admin = true;
                    }
                    HTMLusuarioincidencias($datos, $admin);
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "logs":
                if ($userType === "admin") {
                    $modelologs = new ModeloLogs();
                    $logs = $modelologs->getAllLogs();
                    HTMLpag_logs($logs);
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "gestionbbdd":
                if ($userType === "admin") {
                    HTMLpag_gestionbbdd();
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "gestionusuarios":
                $modeloUsuario = new ModeloUsuario();
                if ($userType === "admin") {
                    $usuario = $modeloUsuario->getAllUsuarios();
                    HTMLpag_gestionusuarios(false, $usuario);
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "listausuarios":
                if ($userType === "admin") {
                    HTMLpag_gestionusuarios(false);
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "pdf":
                $file = 'Views/pdf/tw.pdf';
                $filename = 'tw.pdf';
                header('Content-type:application/pdf');
                header('Content-disposition: inline; filename="'.$filename.'"');
                header('content-Transfer-Encoding:binary');
                header('Accept-Ranges:bytes');
                readfile($file);

                break;
            default:
                HTMLpag_404();
        }

        $modelousuario = new ModeloUsuario();
        $losQuemasComentan = $modelousuario->getlosQueMasComentan();
        $losQuemasAniaden = $modelousuario->getlosqueMasIncidencias();


        switch ($userType) {
            case "anon":
                HTMLaside("", $losQuemasComentan, $losQuemasAniaden);
                break;
            case "admin":
                $modeloUsuario = new ModeloUsuario();
                $imagenPerfil = $modeloUsuario->getProfileImage($_SESSION['user_id']);
                HTMLaside($imagenPerfil, $losQuemasComentan, $losQuemasAniaden);
                break;
            default:
                $modeloUsuario = new ModeloUsuario();
                $imagenPerfil = $modeloUsuario->getProfileImage($_SESSION['user_id']);
                HTMLaside($imagenPerfil, $losQuemasComentan, $losQuemasAniaden);
                break;
        }
        HTMLfooter();
        HTMLclose();
    }
}

?>