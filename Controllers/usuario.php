<?php

require_once "Core/controllerinterface.php";
require_once "Models/modelousuario.php";
require_once "Models/modelologs.php";
require_once "Controllers/login.php";


/**
 * Class Usuario. Clase contrladora de usuario. Se encarga de unir la vista con el modelo.
 */
class Usuario implements ControllerInterface
{


    /**
     * Función principal del controlador. Se encarga de cargar disintas vistas según $controllerParam
     * @param $controllerParam string parámetro que indica que acción realizar
     * @param $requestParams string parámetro extra introducido por la url.
     */
    public function run($controllerParam, $requestParams)
    {

        if(!isset($_SESSION['user_id'])){
            $_SESSION["errormsg"] = "Acceso denegado";
            header("Location: " . Config::BASE_URL . "incidencias");
        }

        $modelousuario = new ModeloUsuario();
        $modelologs = new ModeloLogs();
        switch ($controllerParam) {
            case "create":
                if (Login::checkType("admin")) {
                    if($this->setUser($requestParams, $modelousuario, $modelologs, "gestion/usuarios", true)) {
                        $_SESSION['exitomsg'] = 'Se ha creado el usuario con éxito';
                        header("Location: " . Config::BASE_URL . "gestion/usuarios");
                    }
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . "incidencias");
                }
                break;
            case "update":
                if (empty($requestParams)) {
                    if(isset($_SESSION['usuario'])){
                        header("Location: " . Config::BASE_URL . "usuario/editar/".$_SESSION['user_id']);
                    }else{
                        $_SESSION["errormsg"] = "Acceso denegado";
                        header("Location: " . Config::BASE_URL . "incidencias");
                    }
                }else{
                    if ($modelousuario->usuarioExists((int)$requestParams) && (Login::checkType("admin") || $requestParams === $_SESSION['user_id'])) {

                        if($this->setUser($requestParams, $modelousuario, $modelologs, "usuario/editar/$requestParams", false)){
                            $_SESSION['exitomsg']='Se ha modificado el usuario con éxito';
                            header("Location: " . Config::BASE_URL . "usuario/editar/$requestParams");
                        }
                    } else {
                        $_SESSION["errormsg"] = "Acceso denegado";
                        header("Location: " . Config::BASE_URL . "incidencias");
                    }
                }

                break;
            case "borrar":
                $emailUsuario = '';
                $modelousuario = new ModeloUsuario();
                if(isset($_POST['mail'])) $emailUsuario = $_POST['mail'];

                $id = $modelousuario->getUserIdMail($emailUsuario);

                if($id[0][0] == $_SESSION['user_id']){
                    $_SESSION['errormsg'] = 'Error al borrar usuario, un usuario no se puede borrar a si mismo';
                }
                else{
                    if($emailUsuario != ''){
                        if($modelousuario->deleteUser($emailUsuario)){
                            $_SESSION["exitomsg"] = "Éxito al borrar usuario";
                            $modelologs->addLog("Se ha borrado el usuario " . $emailUsuario, 2, (int) $id);
                        }
                        else{
                            $_SESSION["errormsg"] = "Error al borrar usuario";
                        }
                    }
                    else{
                        $_SESSION["errormsg"] = "Error al borrar usuario";
                    }
                }
                header("Location: " . Config::BASE_URL . "gestion/usuarios?listar=true");
                break;
        }

    }

    /**
     * @param $requestParams
     * @param ModeloUsuario $modelousuario
     * @param ModeloLogs $modelologs
     */
    public function setUser($requestParams, ModeloUsuario $modelousuario, ModeloLogs $modelologs, $ruta, $create)
    {
        if (isset($_POST['nombre']) && isset($_POST['email']) && isset($_POST['apellidos']) &&
            isset($_POST['passwd']) && isset($_POST['passwd2']) && isset($_POST['direccion']) &&
            isset($_POST['tlf']) && isset($_POST['rol']) && isset($_POST['estado'])) {

            // Filtrar parametros de entrada
            $apellidos = filter_var($_POST['apellidos'], FILTER_SANITIZE_STRING);
            $nombre = filter_var($_POST['nombre'], FILTER_SANITIZE_STRING);
            $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
            $passwd = $_POST['passwd'];
            $passwd2 = $_POST['passwd2'];
            $direccion = filter_var($_POST['direccion'], FILTER_SANITIZE_STRING);
            $tlf = filter_var($_POST['tlf'], FILTER_SANITIZE_NUMBER_INT);
            $rol = $_POST['rol'];
            $_POST['estado'] === 'activo' ? $estado = 1 : $estado = 0;

            if($create) {
                if ($modelousuario->existeUsuario($nombre)) {
                    $_SESSION['errormsg'] = 'Ya existe el usuario con nombre ' . $nombre;
                    header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $nombre . "&email=" . $email . "&apellidos=" . $apellidos . "&direccion=" . $direccion . "&tlf=" . $tlf . "&rol=" . $rol . "");
                    return false;
                }
                if ($modelousuario->existeUsuarioMail($email)) {
                    $_SESSION['errormsg'] = 'Ya existe el usuario con email ' . $email;
                    header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $nombre . "&email=" . $email . "&apellidos=" . $apellidos . "&direccion=" . $direccion . "&tlf=" . $tlf . "&rol=" . $rol . "");
                    return false;
                }
                if ($modelousuario->existeUsuarioTelefono($tlf)) {
                    $_SESSION['errormsg'] = 'Ya existe el usuario con telefono ' . $tlf;
                    header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $nombre . "&email=" . $email . "&apellidos=" . $apellidos . "&direccion=" . $direccion . "&tlf=" . $tlf . "&rol=" . $rol . "");
                    return false;
                }
            }


            if (!$this->formatoTelefono($tlf)) {
                $_SESSION['errormsg'] = 'Formato telefono incorrecto.';
                header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $nombre . "&email=" . $email . "&apellidos=" . $apellidos . "&direccion=" . $direccion . "&tlf=" . $tlf . "&rol=" . $rol . "");
                return false;

            }


            // Comprobar contraseñas iguales
            if ($passwd != $passwd2) {
                $_SESSION["errorPassword"] = "Las contraseñas no coinciden";
                header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $nombre . "&email=" . $email . "&apellidos=" . $apellidos . "&direccion=" . $direccion . "&tlf=" . $tlf . "&rol=" . $rol . "");
                return false;
            }


            // Otras comprobaciones previas
            if (empty($apellidos) || empty($nombre) || empty($email) || empty($direccion) || empty($tlf) || ($create && empty($_FILES["imagen"]['tmp_name']))) {
                $_SESSION["errormsg"] = "Se han introducido datos incorrectos o faltan campos";
                header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $nombre . "&email=" . $email . "&apellidos=" . $apellidos . "&direccion=" . $direccion . "&tlf=" . $tlf . "&rol=" . $rol . "");
                return false;
            }

            if(!empty($_FILES["imagen"]['tmp_name'])) {
                $target_dir = "Views/img/usuarios/";
                $imageFileType = strtolower(pathinfo(basename($_FILES["imagen"]["name"]), PATHINFO_EXTENSION));
                $target_file = $target_dir . $nombre . "." . $imageFileType;

                if (getimagesize($_FILES["imagen"]["tmp_name"]) !== false && ($imageFileType === "jpg" || $imageFileType === "png" || $imageFileType === "jpeg"
                        || $imageFileType === "gif")) {
                        if (!move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {
                            $_SESSION["errormsg"] = "Error al subir imagen";
                            header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $nombre . "&email=" . $email . "&apellidos=" . $apellidos . "&direccion=" . $direccion . "&tlf=" . $tlf . "&rol=" . $rol . "");
                            return false;
                        }
                } else {
                    $_SESSION["errormsg"] = "Imagen incorrecta o extensión no aceptada";
                    header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $nombre . "&email=" . $email . "&apellidos=" . $apellidos . "&direccion=" . $direccion . "&tlf=" . $tlf . "&rol=" . $rol . "");
                    return false;
                }
            }



            if($create) {
                $id = $modelousuario->createUser($nombre, $apellidos, $email, $passwd, $rol, $direccion, $tlf, $target_file, $estado);
                $modelologs->addLog("Se ha creado el usuario " . $nombre, 2, $id);
                return true;
            }

            if(!empty($_FILES["imagen"]['tmp_name'])){
                $id = $modelousuario->updateUser($requestParams, $nombre, $apellidos, $email, $passwd, $rol, $direccion, $tlf, $target_file, $estado);
            }else{
                $id = $modelousuario->updateUserData($requestParams, $nombre, $apellidos, $email, $passwd, $rol, $direccion, $tlf, $estado);
            }

            $modelologs->addLog("Se ha modificado el usuario " . $nombre, 2, $id);
            return true;
        }

        $_SESSION["errormsg"] = "No se han detectado todos los campos";
        header("Location: " . Config::BASE_URL . "$ruta?nombre=" . $_POST['nombre'] . "&email=" . $_POST['email'] . "&apellidos=" . $_POST['apellidos'] . "&direccion=" . $_POST['direccion'] . "&tlf=" . $_POST['tlf'] . "&rol=" . $_POST['rol'] . "");
        return false;
    }

    /**
     * Función para comprobar que la lista de palabras  o keywords estén en el formato correcto
     * @param $keywords lista de palabras
     * @return bool true si las palabras están en el formato adecuado.
     */
    private function checkKeywordList($keywords)
    {
        $array = explode(",", $keywords);
        foreach ($array as $value) {
            if (!ctype_alnum($value)) {
                return false;
            }
        }
        return true;
    }

    private function formatoTelefono($tlf){
        return ctype_digit($tlf);
    }

}


