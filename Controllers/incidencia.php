<?php

require_once "Core/controllerinterface.php";
require_once "Models/modeloincidencia.php";
require_once "Models/modelologs.php";
require_once "Models/modelocomentario.php";
require_once "Controllers/login.php";

/**
 * Class Incidencia. Define los métodos para la interacción entre la vista y el modelo.
 */
class Incidencia implements ControllerInterface
{


    /**
     * Función principal del controlador que elige que acción realizar según $controllerParam
     * @param $controllerParam string parámetro que elige que acción del controlador realizar.
     * @param $requestParams string parámetro que se pasa por la url.
     */
    public function run($controllerParam, $requestParams)
    {

        $modeloincidencia = new ModeloIncidencia();
        $modelologs = new ModeloLogs();
        switch ($controllerParam) {
            case "delete":
                if (isset($requestParams) && filter_var($requestParams, FILTER_VALIDATE_INT) && $modeloincidencia->incidenciaExists($requestParams)) {
                    if (Login::checkType("admin") || (Login::checkType("colaborador") && $modeloincidencia->userIsOwner($requestParams, $_SESSION['user_id']))) {
                        $res = $modeloincidencia->borrarIncidencia($requestParams);
                        if ($res) {
                            $modelologs->addLog("Usuario " . $_SESSION['usuario'] . " ha borrado la incidencia " . $requestParams, 6, $_SESSION['user_id']);
                        } else {
                            $_SESSION["errormsg"] = "Error al borrar incidencia";
                        }
                        header("Location: " . Config::BASE_URL . "incidencias/usuario");
                    } else {
                        $_SESSION["errormsg"] = "Acceso denegado";
                        header("Location: " . Config::BASE_URL . '404');
                    }
                } else {
                    $_SESSION["errormsg"] = "No se ha detectado el id de la incidencia a borrar";
                    header("Location: " . Config::BASE_URL . "incidencias");
                }
                break;
            case "update":
                if (isset($_POST['editid']) && $modeloincidencia->incidenciaExists($_POST['editid'])) {
                    $id_incidencia = $_POST['editid'];
                    if (Login::checkType("admin") || (Login::checkType("colaborador") && $modeloincidencia->userIsOwner($id_incidencia, $_SESSION['user_id']))) {
                        $titulo = $descripcion = $lugar = $keywords = '';
                        if (isset($_POST['titulo'])) $titulo = $modeloincidencia->sanitizeString($_POST['titulo']);
                        if (isset($_POST['descripcion'])) $descripcion = $modeloincidencia->sanitizeString($_POST['descripcion']);
                        if (isset($_POST['lugar'])) $lugar = $modeloincidencia->sanitizeString($_POST['lugar']);
                        if (isset($_POST['keywords'])) $keywords = $_POST['keywords'];


                        if(empty($titulo) || empty($descripcion) || empty($lugar) || empty($keywords)){
                            $_SESSION["errormsg"] = "Hay campos vacios";
                            header("Location: " . Config::BASE_URL . "incidencias/editar/" . $id_incidencia);
                            break;
                        }
                        if ($this->checkKeywordList($keywords)) {
                            $modeloincidencia->actualizarIncidencia($_POST['editid'], $titulo, $descripcion, $lugar, $keywords);
                            $_SESSION['exitomsg'] = "Incidencia editada con éxito";
                            $modelologs->addLog("Usuario " . $_SESSION['usuario'] . " ha actualizado la incidencia " . $id_incidencia, 7, $_SESSION['user_id']);
                            header("Location: " . Config::BASE_URL . "incidencias/editar/" . $id_incidencia);
                        }else{
                            $_SESSION["errormsg"] = "La lista de palabras clave introducida no es correcta";
                            header("Location: " . Config::BASE_URL . "incidencias/editar/" . $id_incidencia);
                        }
                    } else {
                        $_SESSION["errormsg"] = "Acceso denegado";
                        header("Location: " . Config::BASE_URL . '404');
                    }
                } else {
                    $_SESSION["errormsg"] = "No se ha detectado el id de la incidencia a editar";
                    header("Location: " . Config::BASE_URL . "incidencias");
                }
                break;
            case "update_estado":
                if (isset($_POST['editid']) && $modeloincidencia->incidenciaExists($_POST['editid'])) {
                    $nuevo_estado = '';
                    $id_incidencia = $_POST['editid'];
                    if (Login::checkType("admin")) {
                        if (isset($_POST['estadoIncidencia'])) {
                            $nuevo_estado = $_POST['estadoIncidencia'];
                        } else {
                            $_SESSION["errormsg"] = "No se ha podido editar el estado de la incidencia";
                        }
                        $modeloincidencia->actualizarEstadoIncidencia($id_incidencia, $nuevo_estado);
                        $modelologs->addLog("Usuario " . $_SESSION['usuario'] . " ha cambiado el estado de la incidencia " . $id_incidencia . " a " . $nuevo_estado, 8, $_SESSION['user_id']);
                        $_SESSION['exitomsg'] = "Estado de la incidencia editada con éxito";
                    } else {
                        $_SESSION["errormsg"] = "Acceso denegado";
                    }
                } else {
                    $_SESSION["errormsg"] = "No se ha detectado el id de la incidencia a editar";
                }
                header("Location: " . Config::BASE_URL . "incidencias/editar/$id_incidencia");
                break;
            case "create":
                if (Login::checkType("admin") || Login::checkType("colaborador")) {
                    if (isset($_POST['titulo']) && isset($_POST['descripcion']) && isset($_POST['lugar']) && isset($_POST['keywords'])) {
                        if ($this->checkKeywordList($_POST['keywords'])) {
                            $titulo = $modeloincidencia->sanitizeString($_POST['titulo']);
                            $descripcion = $modeloincidencia->sanitizeString($_POST['descripcion']);
                            $lugar = $modeloincidencia->sanitizeString($_POST['lugar']);
                            $keywords = $_POST['keywords'];
                            $user_id = $_SESSION['user_id'];

                            $idIncidencia = $modeloincidencia->crearIncidencia($user_id, $titulo, $descripcion, $lugar, $keywords);
                            $modelologs->addLog('Usuario ' . $_SESSION['usuario'] . ' ha creado la incidencia ' . $titulo . " con id " . $idIncidencia, 9, $_SESSION['user_id']);

                            if ($idIncidencia != null) {
                                header("Location: " . Config::BASE_URL . "incidencias/editar/$idIncidencia");
                                $_SESSION["exitomsg"] = "Incidencia creada correctamente";
                            } else {
                                $_SESSION["errormsg"] = "Error al crear incidencia";
                                header("Location: " . Config::BASE_URL . "incidencias/nueva?titulo=$titulo&descripcion=$descripcion&lugar=$lugar&keywords=$keywords");
                            }
                        } else {
                            $_SESSION["errormsg"] = "La lista de palabras clave introducida no es correcta";
                            header("Location: " . Config::BASE_URL . "incidencias/nueva?titulo=".$_POST['titulo']."&descripcion=".$_POST['descripcion']."&lugar=".$_POST['lugar']."&keywords=".$_POST['keywords']."");
                        }
                    } else {
                        $_SESSION["errormsg"] = "Faltan campos por rellenar";
                        header("Location: " . Config::BASE_URL . "incidencias/nueva?titulo=".$_POST['titulo']."&descripcion=".$_POST['descripcion']."&lugar=".$_POST['lugar']."&keywords=".$_POST['keywords']."");
                    }
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "addimage":
                $error = false;
                if (Login::checkType("admin") || (Login::checkType("colaborador") )) {
                    if (isset($_POST['id_incidencia']) && $modeloincidencia->incidenciaExists($_POST['id_incidencia'])) {
                        $id_incidencia = $_POST['id_incidencia'];

                        if (isset($_FILES['imagen'])) {
                            $target_dir = "Views/img/incidencias/";
                            $target_file = $target_dir . basename($_FILES["imagen"]["name"]);
                            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                            if (getimagesize($_FILES["imagen"]["tmp_name"]) !== false && ($imageFileType === "jpg" || $imageFileType === "png" || $imageFileType === "jpeg"
                                    || $imageFileType === "gif")) {
                                if (!file_exists($target_file) && !move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {
                                    $_SESSION["errormsg"] = "Error al subir imagen";
                                    $error = true;
                                    header("Location: " . Config::BASE_URL . "incidencias/editar/" . $id_incidencia);
                                    break;
                                }
                            } else {
                                $error = true;
                                $_SESSION["errormsg"] = "Imagen incorrecta o extensión no aceptada";
                                header("Location: " . Config::BASE_URL . "incidencias/editar/" . $id_incidencia);
                                break;
                            }
                            $modeloincidencia->addImage($id_incidencia, $target_file);
                            $modelologs->addLog("Usuario " . $_SESSION['usuario'] . " ha subido una imagen (" . $target_file . ") a la incidencia " . $id_incidencia, 10, $_SESSION['user_id']);
                            if(!$error) $_SESSION["exitomsg"] = "Se ha subido una imagen con éxito";
                            header("Location: " . Config::BASE_URL . "incidencias/editar/" . $id_incidencia);
                        } else {
                            $_SESSION["errormsg"] = "No se ha podido subir la imagen";
                            header("Location: " . Config::BASE_URL . "incidencias/editar/" . $id_incidencia);
                        }
                    } else {
                        $_SESSION["errormsg"] = "Ha ocurrido un error al subir la imagen";
                        header("Location: " . Config::BASE_URL . "incidencias");
                    }
                } else {
                    $_SESSION["errormsg"] = "Acceso denegado";
                    header("Location: " . Config::BASE_URL . '404');
                }
                break;
            case "votarmas":
                if ($modeloincidencia->incidenciaExists($requestParams)) {
                    $this->votarIncidencia($requestParams, $modeloincidencia, 1);
                    if (isset($_SESSION['usuario'])) {
                        $modelologs->addLog("Usuario " . $_SESSION['usuario'] . " ha votado positivo a la incidencia " . $requestParams, 11, $_SESSION['user_id']);
                    }
                } else {
                    $_SESSION["errormsg"] = "La incidencia indicada no existe";
                }
                break;
            case "votarmenos":
                if ($modeloincidencia->incidenciaExists($requestParams)) {
                    $this->votarIncidencia($requestParams, $modeloincidencia, 0);
                    if (isset($_SESSION['usuario'])) {
                        $modelologs->addLog("Usuario " . $_SESSION['usuario'] . " ha votado negativo a la incidencia " . $requestParams, 12, $_SESSION['user_id']);
                    }
                } else {
                    $_SESSION["errormsg"] = "La incidencia indicada no existe";
                }
                break;
            case "comentarincidencia":
                if (isset($requestParams) && filter_var($requestParams, FILTER_VALIDATE_INT) && $modeloincidencia->incidenciaExists($requestParams)) {
                    if (isset($_POST['nuevocomentario'])) {
                        $modelocomentario = new ModeloComentario();
                        $comentario = $_POST['nuevocomentario'];

                        if (isset($_SESSION['user_id'])) {
                            $user_id = $_SESSION['user_id'];
                        } else {
                            $user_id = str_replace(".", "", $_SERVER['REMOTE_ADDR']);
                        }

                        $res = $modelocomentario->insertarComentario($requestParams, $user_id, $comentario);
                        if ($res) {
                            $modelologs = new ModeloLogs();
                            $modelologs->addLog("El usuario " . $user_id . " ha creado un comentario", 12, $user_id);
                            $_SESSION['exitomsg'] = 'Incidencia comentada con éxito';
                        }else{
                            $_SESSION['exitomsg'] = 'Error al crear usuario';
                        }
                        header("Location: " . $_SERVER['HTTP_REFERER']);
                    } else {
                        $_SESSION['errormsg'] = 'No se ha podido comentar la incidencia. Introduzca un comentario.';
                        header("Location: " . $_SERVER['HTTP_REFERER']);
                    }
                } else {
                    $_SESSION["errormsg"] = "La incidencia indicada no existe";
                }
                break;
            case
            "borrarcomentario":
                $modelocomentario = new ModeloComentario();

                if (isset($requestParams) && filter_var($requestParams, FILTER_VALIDATE_INT) && $modelocomentario->comentarioExists($requestParams)) {

                    if (Login::checkType("admin") || (Login::checkType("colaborador") && $modelocomentario->userIsOwner($requestParams, $_SESSION['user_id']))) {

                        $res = $modelocomentario->borrarComentario($requestParams);
                        if ($res) {
                            $modelologs = new ModeloLogs();
                            $modelologs->addLog("El usuario " . $_SESSION['user_id'] . " ha borrado un comentario", 12, $_SESSION['user_id']);
                            $_SESSION['exitomsg'] = 'Comentario borrado con éxito';
                        } else {
                            $_SESSION['exitomsg'] = 'Error al borrar comentario';
                        }
                        header("Location: " . $_SERVER['HTTP_REFERER']);
                    }else{
                        $_SESSION['errormsg'] = 'Acceso denegado.';
                        header("Location: " . Config::BASE_URL . '404');
                    }
                }else{
                    $_SESSION['errormsg'] = 'No se ha podido detectar el comentario a borrar.';
                    header("Location: " . $_SERVER['HTTP_REFERER']);
                }
                break;

        }

    }

    /**
     * Función que comprueba que la lista de palabras esté en el formato deseado.
     * @param $keywords string palabras clave de la incidencia
     * @return bool true si las keywords están en el formato correcto false en otro caso
     */
    private
    function checkKeywordList($keywords)
    {
        $array = explode(",", $keywords);
        foreach ($array as $value) {
            if (!ctype_alnum($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Función para votar una incidencia.
     * @param $requestParams string Parámetros pasados por la url, en este caso la id incidencia
     * @param ModeloIncidencia $modeloincidencia ModeloIncidencia para gestionar las operaciones con la tabla Incidencias.
     * @param $voto int El voto que ha realizado un usuario, puede ser 0 o 1.
     */
    public
    function votarIncidencia($requestParams, ModeloIncidencia $modeloincidencia, $voto)
    {

        $id_incidencia = (int)$requestParams;
        if ($modeloincidencia->incidenciaExists($id_incidencia)) {
            // Comprobar login
            if (Login::checkType("admin") || (Login::checkType("colaborador"))) {
                $user_id = $_SESSION['user_id'];
                $res = $modeloincidencia->votar($id_incidencia, $user_id, $voto);
            } else {

                // Comprobar cookies
                setcookie("cookieEnabled", "test", time() + 3600, '/');
                if (count($_COOKIE) > 0) {
                    // Establecer user_id a la ip del usuario
                    $user_id = str_replace(".", "", $_SERVER['REMOTE_ADDR']);

                    // Comprobar cookie de voto puesta
                    if (!isset($_COOKIE['votos'])) {
                        $votos = $id_incidencia;
                    } else {
                        $arrayVotos = explode(",", $_COOKIE['votos']);
                        if (in_array($id_incidencia, $arrayVotos)) {
                            $votos = $_COOKIE['votos'];
                            $_SESSION["errormsg"] = "Ya has votado la incidencia";
                            header("Location: " . Config::BASE_URL);
                        } else {
                            $votos = $_COOKIE['votos'] . "," . $id_incidencia;
                        }
                    }
                    $res = $modeloincidencia->votar($id_incidencia, $user_id, $voto);

                    if ($res != null) {
                        setcookie('votos', $votos, time() + (86400 * 365), "/");
                    }

                } else {
                    $_SESSION["errormsg"] = "Necesitas activar las cookies para poder votar";
                    header("Location: " . Config::BASE_URL . '404');
                }
            }

            if ($res != null) {
                header("Location: " . $_SERVER['HTTP_REFERER']);
            } else {
                $_SESSION["errormsg"] = "Error al insertar valoracion";
                header("Location: " . Config::BASE_URL . '404');
            }


        } else {
            $_SESSION["errormsg"] = "ID incidencia incorrecto";
            header("Location: " . Config::BASE_URL . "404");
        }
    }

}

?>