<?php

require_once "Core/controllerinterface.php";
require_once "Models/modelobackup.php";
require_once "Controllers/login.php";

/**
 * Class Backup Controlador Backup.
 * Implementa la interacción entre la vista y el modelo.
 */
class Backup implements ControllerInterface
{

    /**
     * @var array Tablas de la base de datos
     */
    private $tables = ["Usuarios","Incidencias","Valoraciones","Logs","Imagenes","Comentarios"];

    /**
     * Función run del controlador, elige que hacer en cada momento dependiendo
     * del $controllerParam
     * @param $controllerParam parámeto del controlador. Indica que acción realiizar.
     * @param $requestParams parámetro que se indica en los GET.
     */
    public function run($controllerParam, $requestParams)
    {
        if(!isset($_SESSION['user_id'])){
            $_SESSION["errormsg"] = "Acceso denegado";
            header("Location: " . Config::BASE_URL . "incidencias");
        }

        if (Login::checkType("admin")){
            switch ($controllerParam) {
                case "download":
                    $this->download();
                    break;
                case "restore":
                    if(isset($_FILES['ficheroRestore']['name'])){
                        if(!$this->restore()){
                            $_SESSION['errormsg']='No se ha podido restaurar la BBDD';
                        }
                    }else{
                        $_SESSION['errormsg']='No se ha podido restaurar la BBDD, no se encuentra el fichero';
                    }
                    header("Location: " . Config::BASE_URL . "/gestion/bbdd");
                    break;
                case "delete":
                    $this->delete();
                    break;
                case "restaurar":
                    break;
            }
        } else {
            $_SESSION["errormsg"] = "Acceso denegado";
            header("Location: " . Config::BASE_URL);
        }
    }

    /**
     * Función para restaurar la base  de datos a través de un fichero.
     * @param $file fichero para restaurar la base de datos.
     * @return true si ha tenido éxito false en otro caso.
     */
    private function restore()
    {
        $modelobackup = new ModeloBackup();
        $tmplinea = '';

        $sentencias = file($_FILES['ficheroRestore']['tmp_name']);

        foreach ($sentencias as $linea){
            //Saltamos comentarios
            if(substr($linea, 0, 2) == '--' || $linea == ''){
                continue;
            }

            $tmplinea .= $linea;

            if(substr(trim($linea), -1, 1) === ';'){
                //Ejecutamos tmplinea y la limpiamos
                if(!$modelobackup->execute($tmplinea)){
                    $_SESSION['errormsg']='Error al realizar la restauración de la BBDD';
                    return false;
                }
                $tmplinea = '';
            }
        }
        $_SESSION['exitomsg']='Se ha realizado la restauración con éxito';
        return true;
    }

    /**
     * Función para borrar la base de datos.
     */
    private function delete()
    {
        $modelobackup = new ModeloBackup();

        $modelobackup->setFKCheck(false);
        // Loop through the tables
        foreach ($this->tables as $table) {
            $modelobackup->truncate($table);
        }
        $modelobackup->setFKCheck(true);
        header("Location: " .  Config::BASE_URL . "gestion/bbdd");
    }

    /**
     * Función para descargar un fichero con el dump de la base de datos
     * @return bool true si se ha realizado la función con éxito false en otro caso
     */
    private function download(){
        $modelobackup = new ModeloBackup();
        $dump = 'SET FOREIGN_KEY_CHECKS=0;' . "\n";

            // Loop through the tables
            foreach ($this->tables as $table) {

                $tableData = $modelobackup->selectFromTable($table);
                $numColumns = $tableData->columnCount();
                $fetchAll = $tableData->fetchAll();

                // Add DROP TABLE statement$tables
                $dump .= 'DROP TABLE ' . $table . ';' . "\n\n";

                // Add CREATE TABLE statement
                $createTable = $modelobackup->showCreateTable($table);
                $dump .= $createTable['Create Table'] . ';' . "\n\n";

                // Add INSERT INTO statements
                foreach ($fetchAll as $row) {
                    $dump .= "INSERT INTO $table VALUES(";
                    for ($j = 0; $j < $numColumns; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = preg_replace("/\n/us", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $dump .= '"' . $row[$j] . '"';
                        } else {
                            $dump .= '""';
                        }
                        if ($j < ($numColumns - 1)) {
                            $dump .= ',';
                        }
                    }

                    $dump .= ');' . "\n";
                }
                $dump .= "\n\n";
            }
        $dump .= 'SET FOREIGN_KEY_CHECKS=1;' . "\n";

        // Guardar
        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=dump.sql");
        echo $dump;

        return true;
    }

}

?>