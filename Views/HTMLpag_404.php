<?php


/**
 * Función para indicar ruta no encontrada.
 */
function HTMLpag_404(){
    $msg_error = '';

    if(isset($_SESSION['errormsg'])){
        $msg_error = $_SESSION['errormsg'];
        unset($_SESSION['errormsg']);
    }
echo <<< HTML
    <div class="contenidoCentral">
        <main class="contenidoIzquierda">
            <h2>404</h2>
            <h3>ERROR</h3>
            <h3 class="error">$msg_error</h3>    
        </main>
HTML;

}

?>