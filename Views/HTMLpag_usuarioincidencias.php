<?php
require_once "Views/HTMLfiltrar_incidencias.php";

/**
 * Funcion que muestra las incidencias de un usuario
 * @param $datos datos de la incidencia a mostrar.
 */
function HTMLusuarioincidencias($datos,$admin=false)
{

    echo "<div class='contenidoCentral'>";
    echo "<main class='contenidoIzquierdaIncidencias'>";
    if($admin){
        $action='incidencias/usuario';
        echo "<form id='formin2' action=\"" . Config::BASE_URL . "$action\"  method=\"post\">";
        echo <<< HTML
         <select name="formCuantasIncidencias" form="formin2">
              <option selected  value="todas">Todas las incidencias</option>
              <option  value="tusincidencias">Tus incidencias</option>
         </select>
         <button type="submit">Mostrar incidencias</button> 
HTML;
        echo "</form>";
    }
    if (empty($datos)) {
        echo"<h2>No tienes incidencias</h2>";
    } else {
        foreach ($datos as $dato) {
            $id_incidencia = $dato['incidencia']['id'];
            $titulo = $dato['incidencia']['titulo'];
            $lugar = $dato['incidencia']['lugar'];
            $keywords = $dato['incidencia']['keywords'];
            $fecha = $dato['incidencia']['fecha'];
            $estado = $dato['incidencia']['estado'];
            $descripcion = $dato['incidencia']['descripcion'];
            $autor = $dato['incidencia']['nombre'];
            $imagenes = $dato['imagenes'];
            $comentarios = $dato['comentarios'];

            if (!empty($dato['valoraciones'])) {
                if (isset($dato['valoraciones'][0]['positivas'])) {
                    $valoraciones_positivas = $dato['valoraciones'][0]['positivas'];

                } else {
                    $valoraciones_positivas = 0;
                }
                if (isset($dato['valoraciones'][0]['negativas'])) {
                    $valoraciones_negativas = $dato['valoraciones'][0]['negativas'];
                } else {
                    $valoraciones_negativas = 0;
                }
            } else {
                $valoraciones_positivas = 0;
                $valoraciones_negativas = 0;
            }

            $estadoString='';
            switch ($estado){
                case 0:
                    $estadoString='Pendiente';
                    break;
                case 1:
                    $estadoString='Comprobada';
                    break;
                case 2:
                    $estadoString='Tramitada';
                    break;
                case 3:
                    $estadoString='Irresoluble';
                    break;
                case 4:
                    $estadoString='Resuelta';
                    break;

            }

            echo "<article class='incidencias'>";
            echo "<div class='encabezadoIncidencia'>";
            echo "<h2>$titulo</h2>";
            echo "<div class='infoIncidencia'>";
            echo "<p>Lugar: $lugar</p>";
            echo "<p class='itemIncidencia'>Fecha: $fecha</p>";
            echo "<p class='itemIncidencia'>Creado por: $autor</p>";
            echo "</div>";
            echo "<div class='infoIncidencia'>";
            echo "<p>Palabras clave: $keywords</p>";
            echo "<p class='itemIncidencia'>Estado: $estadoString</p>";
            echo "<p class='itemIncidencia'>Valoraciones: +" . $valoraciones_positivas . " -" . $valoraciones_negativas . "</p>";
            echo "</div>";
            echo "<div class='comentarioIncidencia'>";
            echo "</div>";
            echo "</div>";

            foreach ($imagenes as $imagen) {

                $img = $imagen['imagen'];
                echo "<img class='imagenIncidencia' src=\"" . Config::BASE_URL . "$img\" alt=\"imagenincidencia\">";
            }
            echo "<div class='botonesIncidencias'>";
            echo "<form>";
            echo "<button formaction=\"" . Config::BASE_URL . "incidencias/votarmas/" . $id_incidencia . "\"><img alt=\"votarmas\" src=\"" . Config::BASE_URL . "img/botonMasComentario.png\"\></button>";
            echo "<button formaction=\"" . Config::BASE_URL . "incidencias/votarmenos/" . $id_incidencia . "\"><img alt=\"votarmenos\" src=\"" . Config::BASE_URL . "img/botonMenosComentario.png\"\></button>";
            echo "<button formaction=\"" . Config::BASE_URL . "incidencias/editar/" . $id_incidencia . "\"><img alt=\"editarincidencia\" src=\"" . Config::BASE_URL . "img/botoneditarComentario.png\"\></button>";
            echo "<button formaction=\"" . Config::BASE_URL . "incidencias/delete/" . $id_incidencia . "\"><img alt=\"borrarincidencia\" src=\"" . Config::BASE_URL . "img/papeleraIcono.png\"\></button>";
            echo "</form>";
            echo "</div>";
            echo "<div class='descripcionIncidencia'>";
                echo "<p>$descripcion</p>";
            echo "</div>";
            foreach ($comentarios as $comentario) {
                $texto = $comentario['texto'];
                isset($comentario['nombre']) ? $nombre = $comentario['nombre'] : $nombre = "Anonimo";
                echo "<div class=comentarioIncidencia>";
                echo "<p class='idautorcomentario'>Autor: " . $nombre . "</p>";
                echo "<form class='idformcomentario'>";
                echo "<button class='botonBorrarIncidencia' formaction=''><img alt=\"borrarcomentario\" src=\"" . Config::BASE_URL . "img/papeleraIcono.png\"/></button>";
                echo "</form>";
                echo "<p class='textoComentario'>$texto</p>";
                echo "</div>";
            }

            echo "</article>";
        }
    }
    echo "</main>";
}

?>
