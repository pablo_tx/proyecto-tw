<?php

/**
 * Función con los bloques aside, a la derecha de la página.
 * Muestra el Login para que un usuario pueda hacer log in en la página.
 */
function HTMLasideLogin()
{
    echo "<aside id=\"formLogin\">";
    echo "     <form action=\"" . Config::BASE_URL . "login\" method=\"post\">";
    echo "      <div class=\"imgcontainer\">";

    echo "          <img src=\"" . Config::BASE_URL . "img/profile.png\" alt=\"Avatar\" id=\"avatarImg\">";
    echo <<< HTML
                </div>
                <div class="loginContainer">
                    <label for="user"><b>Username</b></label>
                    <input type="text" placeholder="Enter Username" name="user" required>
    
                    <label for="pass"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="pass" required>
    
                    <button id="botonLogin" type="submit">Login</button>
                </div>
            </form>
        </aside>
HTML;
}

/**
 * Función que muestra el perfil del usuario con la sesión iniciada.
 * Muestra la foto de perfil del usuario así como dos botones, uno de editar y otro de log out.
 * @param $imagen imagen de perfil del usuario.
 */
function HTMLasidePerfilUsuario($imagen)
{
    echo "<aside id=\"formLogin\">";
    echo "     <form action=\"" . Config::BASE_URL . "logout\" method=\"post\">";
    echo "      <div class=\"imgcontainer\">";

    echo "          <img src=\"" . Config::BASE_URL . $imagen."\" alt=\"Avatar\" id=\"avatarImg\">";
    echo <<< HTML
                </div>
                <div id="botonesPerfilUsuario">
                    <button id="botonLogout" type="submit">Logout</button>
HTML;
    echo "<button id='botonEditarPerfil' type=\"submit\" formaction=\"" . Config::BASE_URL . "usuario/editar\">Editar</button>";
    echo <<< HTML
                </div>
            </form>
        </aside>
HTML;
}

/**
 * Función principal del bloque aside de la derecha de la página.
 * Invoca a las subfunciones según si hay una sesión de usuario iniciada o no.
 * @param $imagen
 */
function HTMLaside($imagen, $mascomentan, $masaniaden)
{
    echo <<< HTML
    <div class="rightSideInicio">
HTML;
    if (isset($_SESSION['usuario'])) {
        HTMLasidePerfilUsuario($imagen);
    } else {
        HTMLasideLogin();
    }

    /*if (isset($_SESSION["errormsg"])) {
        $error = $_SESSION["errormsg"];
        echo "<h3 id='errormsg'>$error</h3>";
        unset($_SESSION["errormsg"]);
    }*/

    echo <<< HTML
    <aside id="populares">
        <h2>Los que más añaden</h2>
            <article>
                <ul>
HTML;

    foreach ($masaniaden as $usuario => $c){
        $usuario = $c['nombre'];
        echo "<li>$usuario</li>";
    }
    echo <<< HTML
                </ul>
             </article>
         <h2>Los que más opinan</h2>
            <article>
                <ul>
HTML;
    foreach ($mascomentan as $usuario => $c){
        $usuario = $c['nombre'];
        echo "<li>$usuario</li>";
    }
        echo <<< HTML
                </ul>
            </article>
        </aside>
        </div>
    </div>
HTML;
}

?>