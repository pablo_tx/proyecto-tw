<?php

function HTMLpag_restaurar(){
    echo <<< HTML
    <div>
        <h3>Indique el archivo con la copia de seguridad que desea restaurar (sintaxis SQL)</h3>
HTML;
    echo "<form action=\"" . Config::BASE_URL . "gestion/bbdd/restaurar\" method=\"post\" enctype=\"multipart/form-data\">";
    echo <<< HTML
        <input type="file" name="ficheroRestore" required>
        <button type="submit">Restaurar BBDD</button>
    </form>
    </div>
HTML;
}

function HTMLpag_borrar(){
    echo <<< HTML
    <div>
        <h3>Va a proceder al borrado de la BBDD completa. Si pulsa el botón no hay forma de restaurar la información</h3>
        <h3>¿Está seguro de que desea borrar la BBDD?</h3>
HTML;
echo "<form action=\"" . Config::BASE_URL . "gestion/bbdd/borrar\" method=\"post\" enctype=\"multipart/form-data\">";
    echo <<< HTML
    </div>
    <button type="submit">Borrar la BBDD</button>
</form>
HTML;
}
/**
 * Función para generar la vista de la gestión de la Base de datos.
 */
function HTMLpag_gestionbbdd(){
    echo"    <link rel=\"stylesheet\" href=\"" . Config::BASE_URL . "css/gestionbbdd.css\">";
    echo <<< HTML
    <div class="contenidoCentral">
        <main class="contenidoIzquierda">
            <div>
                <h1>Gestión de la BBDD</h1>
                <h2>Indique la acción a realizar</h2>
            </div>

            <div class="accionesBDD">
                <nav class="navBDD">
                    <ul>
HTML;

echo"                        <li><a href=\"" . Config::BASE_URL ."gestion/bbdd/descargar\">Descargar copia de seguridad</a></li>";
echo"                        <li><a href=\"" . Config::BASE_URL ."gestion/bbdd/?restaurar=true\">Restaurar copia de seguridad</a></li>";
echo"                        <li><a href=\"" . Config::BASE_URL ."gestion/bbdd/?borrar=true\">Borrar la BBDD</a></li>";
    echo <<< HTML
                    </ul>
                </nav>
            </div>
HTML;
    if(isset($_GET['restaurar'])){
        HTMLpag_restaurar();
    }else if(isset($_GET['borrar'])){
        HTMLpag_borrar();
    }
    echo <<< HTML
        </main>
HTML;
}

?>