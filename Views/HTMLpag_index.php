<?php
require_once "Views/HTMLfiltrar_incidencias.php";


/**
 * Función para comentar una incidencia.
 * @param $id_incidencia id de la incidencia que se va a comentar
 */
function HTMLcomentar($id_incidencia){
    echo "<form action=\"" . Config::BASE_URL . "incidencias/comentar/" . $id_incidencia . "\">";
    echo <<< HTML
        <div class="divComentar">
            <input type='text' name='comentario'>
            <button type="submit">Comentar</button>
        </div>
    </form>
HTML;
}
/**
 * Función que genera el índice principal de la página.
 * Es la página inicial al entrar en la aplicación.
 * @param $datos datos de todas las incidencias.
 */
function HTMLpag_index($datos){
    $exito = $error = '';

    if(isset($_SESSION['errormsg'])){
        $error = $_SESSION['errormsg'];
        unset($_SESSION['errormsg']);
    }
    if(isset($_SESSION['exitomsg'])){
        $exito = $_SESSION['exitomsg'];
        unset($_SESSION['exitomsg']);
    }
    echo <<< HTML
    <div class="contenidoCentral">
        <main class="contenidoIzquierdaIncidencias">
        
    <h2 class="exito">$exito</h2>
    <h2 class="error">$error</h2>
HTML;

    HTMLfiltrar_incidencias();
    foreach ($datos as $dato) {
        $id_incidencia = $dato['incidencia']['id'];
        $titulo = $dato['incidencia']['titulo'];
        $lugar = $dato['incidencia']['lugar'];
        $keywords = $dato['incidencia']['keywords'];
        $fecha = $dato['incidencia']['fecha'];
        $estado = $dato['incidencia']['estado'];
        $descripcion = $dato['incidencia']['descripcion'];
        $usuario = $dato['incidencia']['nombre'];
        $id_usuario = $dato['incidencia']['id_usuario'];
        $imagenes = $dato['imagenes'];
        $comentarios = $dato['comentarios'];

        if (!empty($dato['valoraciones'])) {
            if (isset($dato['valoraciones'][0]['positivas'])) {
                $valoraciones_positivas = $dato['valoraciones'][0]['positivas'];

            } else {
                $valoraciones_positivas = 0;
            }
            if (isset($dato['valoraciones'][0]['negativas'])) {
                $valoraciones_negativas = $dato['valoraciones'][0]['negativas'];
            } else {
                $valoraciones_negativas = 0;
            }
        } else {
            $valoraciones_positivas = 0;
            $valoraciones_negativas = 0;
        }

        $estadoString='';
        switch ($estado){
            case 0:
                $estadoString='Pendiente';
                break;
            case 1:
                $estadoString='Comprobada';
                break;
            case 2:
                $estadoString='Tramitada';
                break;
            case 3:
                $estadoString='Irresoluble';
                break;
            case 4:
                $estadoString='Resuelta';
                break;
        }

        echo "<article class='incidencias'>";
        echo "<div class='encabezadoIncidencia'>";
        echo "<h2>$titulo</h2>";
        echo "<div class='infoIncidencia'>";
        echo "<p>Lugar: $lugar</p>";
        echo "<p class='itemIncidencia'>Fecha: $fecha</p>";
        echo "<p class='itemIncidencia'>Creado por: $usuario</p>";
        echo "</div>";
        echo "<div class='infoIncidencia'>";
        echo "<p>Palabras clave: $keywords</p>";
        echo "<p class='itemIncidencia'>Estado: $estadoString</p>";
        echo "<p class='itemIncidencia'>Valoraciones: +" . $valoraciones_positivas . " -" . $valoraciones_negativas . "</p>";
        echo "</div>";
        echo "<div class='comentarioIncidencia'>";
        echo "</div>";
        echo "</div>";

        foreach ($imagenes as $imagen) {

            $img = $imagen['imagen'];
            echo "<img class=\"imagenIncidencia\" src=\"" . Config::BASE_URL . "$img\" alt=\"imagenincidencia\">";
        }
        echo "<div class='botonesIncidencias'>";
        echo "<form>";
        echo "<button formaction=\"" . Config::BASE_URL . "incidencias/votarmas/" . $id_incidencia . "\"><img alt=\"votarmas\" src=\"" . Config::BASE_URL . "img/botonMasComentario.png\"\></button>";
        echo "<button formaction=\"" . Config::BASE_URL . "incidencias/votarmenos/" . $id_incidencia . "\"><img alt=\"votarmenos\" src=\"" . Config::BASE_URL . "img/botonMenosComentario.png\"\></button>";
        echo "<button formaction=\"" . Config::BASE_URL . "incidencias/editar/" . $id_incidencia . "\"><img alt=\"editarincidencia\" src=\"" . Config::BASE_URL . "img/botoneditarComentario.png\"\></button>";
        echo "<button formaction=\"" . Config::BASE_URL . "incidencias/delete/" . $id_incidencia . "\"><img alt=\"borrarincidencia\" src=\"" . Config::BASE_URL . "img/papeleraIcono.png\"\></button>";
        echo "</form>";
        echo "</div>";

        echo "<div class='descripcionIncidencia'>";
        echo "<p>$descripcion</p>";
        echo "</div>";

        foreach ($comentarios as $comentario) {
            $texto = $comentario['texto'];
            $id = $comentario['id'];
            isset($comentario['nombre']) ? $nombre = $comentario['nombre'] : $nombre = "Anonimo";
            echo "<div class='comentarioIncidencia'>";
            //echo "<div class='autoryboton'>";
            echo "<p class='idautorcomentario' >Autor: " . $nombre . "</p>";
            echo "<form class='idformcomentario' action=\"" . Config::BASE_URL . 'incidencias/borrarcomentario/'.$id. "\" method=\"post\">";
            echo "<button type='submit' class='botonBorrarIncidencia'><img alt=\"borrarcomentario\" src=\"" . Config::BASE_URL . "img/papeleraIcono.png\"/></button>";
            echo "</form>";
            //echo "</div>";
            echo "<p class='textoComentario'>$texto</p>";
            echo "</div>";
        }

        echo "          <form id='comentarIncidencia' action=\"" . Config::BASE_URL . "incidencias/comentarincidencia/".$id_incidencia."\"  method=\"post\">";
             echo <<< HTML
            <input name='nuevocomentario' type="text">
HTML;
echo "<button type='submit' >Comentar Incidencia</button>";
        echo <<< HTML
        </form>     

HTML;


        echo "</article>";
    }
        echo <<< HTML
        </main>
HTML;
}

?>