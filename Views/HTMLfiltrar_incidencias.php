<?php

/**
 * Función que muestra el formulario inicial para filtrar las incidencias.
 */
function HTMLfiltrar_incidencias(){
    $action = '';
        echo <<< HTML
        <h2>Criterios de búsqueda</h2>
            <div class="filtrarIncidencias">
                <h3>Ordenar por:</h3>
HTML;
    echo "<form action=\"" . Config::BASE_URL. $action. "\" method=\"post\">";
    echo <<< HTML
                    <div id="ordenarContainer">
                        <div>
                            <div>
                                <input type="radio" name="filtro" value="antiguedad" checked>
                                <label for="antiguedad">Antigüedad (primero los más recientes)</label>                  
                            </div>
                            <div>
                                <input type="radio" name="filtro" value="positivosnetos" >
                                <label for="positivosnetos">Número de positivos netos (de más a menos)</label>                    
                            </div>
                        </div>   
                        <div>
                                <input type="radio" name="filtro" value="positivos" >
                                <label for="positivos">Número de positivos (de más a menos)</label>                        
                        </div>             
                    </div>
     
                    <h3>Incidencias que contengan:</h3>
                    <div class="formularioContainer">
                        <div>
                            <label for="textobusqueda">Texto de búsqueda</label>
                            <input type="text" name="textobusqueda">
                        </div>
                        <div>
                            <label for="lugar"><b>Lugar</b></label>
                            <input type="text" name="lugar">
                        </div>
                    </div>
                    <h3>Estado:</h3>
                    <div class="formularioEstadoIncidencia">
                        <div>
                            <input type="radio" name="estado" value="Pendiente" >
                            <label for="pendiente">Pendiente</label>
                        </div>
            
                        <div>
                            <input type="radio" name="estado" value="Comprobada">
                            <label for="comprobada">Comprobada</label>              
                        </div>      
                        
                        <div>         
                            <input type="radio" name="estado" value="Tramitada">
                            <label for="tramitada">Tramitada</label>
                        </div>        
                        
                        <div>
                            <input type="radio" name="estado" value="Irresoluble">
                            <label for="irresoluble">Irresoluble</label>
                        </div>   
            
                        <div>
                            <input type="radio" name="estado" value="Resuelta">
                            <label for="resuelta">Resuelta</label>
                        </div>       
                    </div>
                    <button type="submit">Aplicar criterios de búsqueda</button>
                </form>
            </div>
HTML;
    }
?>