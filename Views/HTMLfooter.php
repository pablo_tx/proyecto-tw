<?php


/**
 * Función que genera el footer de la página.
 */
function HTMLfooter(){
echo <<< HTML
    <footer> 
        <h3>&copy; Tecnologías Web 2019</h3>
        <h3>Víctor Moreno Jiménez</h3>
        <h3>Pablo Pozo Tena</h3> 
HTML;
        echo "<a href=\"" . Config::BASE_URL . "pdf\" target='_blank'>PDF</a>";
        echo <<< HTML
        
    </footer>
HTML;
}

?>