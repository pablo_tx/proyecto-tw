<?php

require_once "Views/HTMLpag_nuevaincidencia.php";


/**
 * Función que muestra el formulario al editar una incidencia, el bloque superior de estado.
 * Este muestra la información original de la incidencia que va a ser editada, en este caso el estado.
 * @param $estado parámetro que indica el estado de la incidencia. Pendiente, Tramitada, Comprobada, Irresouluble o Resuelta.
 * @param $id_incidencia id incidencia de la incidencia a editar.
 */
function HTMLeditar_incidencia_estado($estado, $id_incidencia, $datos){
    $msg_exito = $msg_error = '';
    if(isset($_SESSION['exitomsg'])){
        $msg_exito = $_SESSION['exitomsg'];
        unset($_SESSION['exitomsg']);
    }
    if(isset($_SESSION['errormsg'])){
        $msg_error = $_SESSION['errormsg'];
        unset($_SESSION['errormsg']);
    }
    
    $estado_boton='disabled';
    if (Login::checkType("admin")){
        $estado_boton='';
    }
    $checkedPendiente=$checkedComprobada=$checkedTramitada=$checkedIrresoluble=$checkedResuelta='';
    switch ($estado){
        case 0:
            $checkedPendiente = 'checked';
            break;
        case 1:
            $checkedComprobada = 'checked';
            break;
        case 2:
            $checkedTramitada = 'checked';
            break;
        case 3:
            $checkedIrresoluble = 'checked';
            break;
        case 4:
            $checkedResuelta = 'checked';
            break;
    }
    echo <<< HTML
    <h2>Estado de la incidencia:</h2>
HTML;

echo "<form action=\"" . Config::BASE_URL . "incidencias/update_estado/\" method=\"post\">";
        echo <<< HTML
        <div class="formularioEstadoIncidencia">
            <div>
                <input type="radio" name="estadoIncidencia" value="Pendiente" $checkedPendiente>
                <input type="hidden" name="editid" value=$id_incidencia>
                <label for="pendiente">Pendiente</label>
            </div>

            <div>
                <input type="radio" name="estadoIncidencia" value="Comprobada" $checkedComprobada>
                <label for="comprobada">Comprobada</label>
            </div>      
            
            <div>
                <input type="radio" name="estadoIncidencia" value="Tramitada" $checkedTramitada>
                <label for="tramitada">Tramitada</label>
            </div>        
            
            <div> 
                <input type="radio" name="estadoIncidencia" value="Irresoluble" $checkedIrresoluble>
                <label for="irresoluble">Irresoluble</label>
            </div>   

            <div>
                <input type="radio" name="estadoIncidencia" value="Resuelta" $checkedResuelta>
                <label for="resuelta">Resuelta</label>
            </div>
             <!--//TODO si //Rellenar datos datos=null;eres admin el boton se activa-->
        </div>
        
        <h3 class="error">$msg_error</h3>
        <h3 class="exito">$msg_exito</h3>
        
        <button id="botonEditarIncidencia" type="submit" $estado_boton>Modificar estado</button>
    </form>
HTML;
}

/**
 * Segundo formulario de la vista editar incidencia.
 * En este caso muestra la opción de subir una fotografía a la incidencia.
 * @param $id identificador de la incidencia que se va a editar.
 */
function HTMLeditar_incidencia_imagen($id){
    echo <<< HTML
    <h2>Fotografías adjuntas:</h2>
HTML;
    echo "<form id=\'formularioFotografia\' action=\"" . Config::BASE_URL . "incidencias/addimage\" method=\"post\" enctype=\"multipart/form-data\">";
    echo "<input type=\"hidden\" value=\"$id\" name=\"id_incidencia\">";
    echo <<< HTML
        <input type="file" name="imagen" id="fotoUpload" required>
        <button type="submit">Añadir una fotografía</button>
    </form>
HTML;
}


/**
 * Funcion principal de la vista de editar incidencia.
 * Ésta función invoca a las tres funciones secundarias con cada uno de los sub-formularios
 * @param $datos datos de la incidencia que se va a editar.
 */
function HTMLeditar_incidencia($datos){
    $msg_exito = $msg_error = '';
    if(isset($_SESSION['exitomsg'])){
        $msg_exito = $_SESSION['exitomsg'];
        unset($_SESSION['exitomsg']);
    }
    if(isset($_SESSION['errormsg'])){
        $msg_error = $_SESSION['errormsg'];
        unset($_SESSION['errormsg']);
    }

    echo <<< HTML
    <div class="contenidoCentral">
        <main class="contenidoIzquierda">
        <h2>Editar incidencia</h2>
        <h3 class="error">$msg_error</h3>
        <h3 class="exito">$msg_exito</h3>

HTML;

echo "<div id='bloqueEditarIncidencia'>";
echo    "<div class='formularioBloque'>";
            HTMLform_incidencia($datos);
echo    "</div>";
echo    "<div class='formularioBloque'>";
            HTMLeditar_incidencia_estado($datos['estado'],$datos['id'],$datos);
echo    "</div>";
echo    "<div class='formularioBloque'>";
            HTMLeditar_incidencia_imagen($datos['id']);
echo    "</div>";
echo "</div>";
            echo <<< HTML
        </main>
HTML;
}

?>
