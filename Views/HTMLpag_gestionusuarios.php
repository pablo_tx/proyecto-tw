<?php

/*
function HTMLlistar_usuarios(){
    echo <<< HTML
    <div class="listarUsuarios">
    <div id="imagenPerfilUsuario">
HTML;
        echo "<img src=\"".Config::BASE_URL."img/profile.png\" \>";
        echo <<< HTML
    </div>
    <div id="datosUsuario">
        <div class="datosUsuarioSecundario">
            <p>Usuario: JuanLuis</p>
            <p>Dirección: calle falsa 123</p>
            <p>Rol: Administrador</p>
        </div>
        <span id="separator"></span>
        <div id="datosUsuarioDerecha">
            <p>Email: mail@falso.com</p>
            <p>Teléfono: 5464545354</p>
            <p>Estado: Activo</p>
        </div>
    </div>

    <div id="botonesEditarBorrarUsuario">
HTML;
        echo "<button action=''><img src=\"".Config::BASE_URL."img/botoneditarComentario.png\"\></button>";
        echo "<button action=''><img src=\"".Config::BASE_URL."img/papeleraIcono.png\"\></button>";

        echo <<< HTML
    </div>
</div>
HTML;
}
*/


/**
 * Formulario para gestionar el alta de un nuevo usuario.
 * También funciona como función para editar el usuario, reutilizando el código.
 * @param $editar true en caso de que estemos editando el usuario false en caso contrario.
 */
function HTMLnuevo_usuario($editar, $admin=false, $datosUsuario=null){
    $nombre = $apellidos = $foto = $email = $clave = $direccion = $telefono = $rol = $imagen = '';

    if($datosUsuario != null){
        $id = $datosUsuario[0]['id'];
        $nombre = $datosUsuario[0]['nombre'];
        $apellidos = $datosUsuario[0]['apellidos'];
        $email = $datosUsuario[0]['email'];
        $clave = '';
        $direccion = $datosUsuario[0]['direccion'];
        $telefono = $datosUsuario[0]['telefono'];
        $rol = $datosUsuario[0]['tipo'];
        $imagen = $datosUsuario[0]['imagen'];
    }

    $accionSelect = 'disabled';
    if($admin || !$editar){
        $accionSelect = '';
    }

    $passwdErr = '';
    if($editar){
        $titulo = "Editar Usuario";
        $action = "gestion/usuario/update/$id";
    } else {
        $titulo = "Nuevo Usuario";
        $action = "gestion/usuario/create";
    }
    if(isset($_SESSION['errorPassword'])){
        $passwdErr = $_SESSION['errorPassword'];
        unset($_SESSION['errorPassword']);
    }

    if(isset($_GET['foto'])) $foto = $_GET['foto'];
    if(isset($_GET['nombre'])) $nombre = $_GET['nombre'];
    if(isset($_GET['apellidos'])) $apellidos = $_GET['apellidos'];
    if(isset($_GET['email'])) $email = $_GET['email'];
    if(isset($_GET['direccion'])) $direccion = $_GET['direccion'];
    if(isset($_GET['tlf'])) $telefono = $_GET['tlf'];
    if(isset($_GET['rol'])) $rol = $_GET['rol'];

    echo <<< HTML
    <div class="nuevoUsuario">
        <h1>$titulo</h1>

        <div class="formularioContainer">
HTML;
    echo "<form action=\"" . Config::BASE_URL . $action. "\" method=\"post\" enctype=\"multipart/form-data\">";
        echo <<< HTML
                <div id="fotoSection">
                    <label for="imagen"><b>Foto: </b></label>
HTML;
        if($imagen != ''){
echo "    <img id='imagenPerfilUsuarioEditar' src=\"" . Config::BASE_URL . "$imagen\" alt=\"imagenincidencia\">";
        }
        echo <<< HTML
                    <input type="file" name="imagen" id="fotoUpload" value='$foto' >
                </div>

                <div>
                    <label for="nombre"><b>Nombre: </b></label>
                    <input type="text" name="nombre" value=$nombre >                    
                </div>

                <div>
                    <label for="apellidos"><b>Apellidos: </b></label>
                    <input type="text" name="apellidos"  value=$apellidos >                    
                </div>

                <div>
                    <label for="email"><b>Email: </b></label>
                    <input type="email" name="email" value=$email >                    
                </div>

                <div>
                    <label for="passwd"><b>Clave: </b></label>
                    <div id="passwordContainer">
                        <input type="password" name="passwd" value=$clave>
                        <span id="separatorPassword"></span>
                        <input type="password" name="passwd2" value=$clave >
                    </div>
                    <div class="error">$passwdErr</div>
                </div>

                <div>
                    <label for="direccion"><b>Dirección: </b></label>
                    <input type="text" name="direccion" value=$direccion>                    
                </div>

                <div>
                    <label for="tlf"><b>Teléfono: </b></label>
                    <input type="text" name="tlf"  value=$telefono>                    
                </div>

                <div>
                    <label for="estado"><b>Estado: </b></label>
                    <select name="estado">
                        <option value="activo">Activo</option>
                        <option value="inactivo">Inactivo</option>
                    </select>
                </div>

                <div>
HTML;
                $activoAdmin = $activoColaborador = '';
                if($rol == 1){
                    $activoAdmin = 'selected';
                } else {
                    $activoColaborador = 'selected';
                }
                echo <<< HTML
                    <label for="rol"><b>Rol: </b></label>
                    <select name="rol">
                        <option $activoColaborador value="0">Colaborador</option>
                        <option $activoAdmin $accionSelect value="1">Administrador</option>
                    </select>
                </div>

                <button type="submit">$titulo</button>

            </form>
        </div>
    </div>
    

HTML;
}

/**
 * Función para borrar un usuario con mail $mail.
 * @param string $mail del usuario que se va a borrar.
 */
function HTMLpag_borrarusuario(){
    $usuario = '';
    if($_GET['mail']){
        $usuario = $_GET['mail'];
    }
    else{
        $_SESSION['errormsg']='No se ha podido borrar al usuario ' . $usuario;
        header("Location: " . Config::BASE_URL . "gestion/usuarios/?listar=true" );
    }
    echo <<< HTML
    <div id="formSeguroBorrar">
        <h3>Va a proceder al borrado del usuario $usuario</h3>
        <h3>¿Está seguro de que desea borrar el usuario?</h3>
HTML;
    echo "<form action=\"" . Config::BASE_URL . "gestion/usuario/borrar\" method=\"post\" enctype=\"multipart/form-data\">";
    echo "<input type='hidden' name='mail' value=$usuario>";
    echo <<< HTML
    <button type="submit">Borrar usuario</button>
    </div>
    
</form>
HTML;
}

/**
 * @param $usuarios
 */
function HTMLlistar_usuarios($usuarios){
    if (!empty($usuarios)){
        if(isset($_GET['borrar'])){
            HTMLpag_borrarusuario();
        }
        foreach ($usuarios as $usuario){
            $nombre = $id = $email = $tipo = $direccion = $telefono = $imagen = $estado = $tipo_string = $estado_string = '';
            if(isset($usuario['id'])) $id = $usuario['id'];
            if(isset($usuario['nombre'])) $nombre = $usuario['nombre'];
            if(isset($usuario['email'])) $email = $usuario['email'];
            if(isset($usuario['tipo'])) $tipo = $usuario['tipo'];
            if(isset($usuario['direccion'])) $direccion = $usuario['direccion'];
            if(isset($usuario['telefono'])) $telefono = $usuario['telefono'];
            if(isset($usuario['imagen'])) $imagen = $usuario['imagen'];
            if(isset($usuario['estado'])) $estado = $usuario['estado'];

            switch ($tipo){
                case 0:
                    $tipo_string = 'Colaborador';
                    break;
                case 1:
                    $tipo_string = 'Admin';
                    break;
            }

            switch ($estado){
                case 0:
                    $estado_string = 'Inactivo';
                    break;
                case 1:
                    $estado_string = 'Activo';
                    break;
            }

            echo <<< HTML
    <div class="bloqueListarUsuarios">
        <div class="fotoListarUsuario">
HTML;
echo "       <img class=\"imgUsuario\" src=\"" . Config::BASE_URL . "$imagen\" alt=\"imagenincidencia\">";
            echo <<< HTML
        </div>
        <div class="datosListarUsuario">
            <div class="primerBloque">
                <p>Usuario: $nombre</p>
                <p>Dirección: $direccion</p>
                <p>Rol: $tipo_string</p>
            </div>
            <div class="segundoBloque">
                <p>Email: $email</p>
                <p>Teléfono: $telefono</p>
                <p>Estado: $estado_string</p>
            </div>
        </div>
        <div class="botonesListarUsuario">
HTML;
            $action = 'gestion/usuario/borrar';
            echo "<form action=\"" . Config::BASE_URL . $action. "\" method=\"post\" \">";
            echo "<input type='hidden' name='mail' value=$email>";
            echo "<a href=\"".Config::BASE_URL."usuario/editar/".$id."\"><img src=\"".Config::BASE_URL."img/botoneditarComentario.png\" alt=\"botoneditar\"></a>";
            echo "<a href=\"".Config::BASE_URL."gestion/usuarios?borrar=true&listar=true&mail=$email\"><img src=\"".Config::BASE_URL."img/papeleraIcono.png\" alt=\"botonborrar\"></a>";
            echo "</form>";
            echo <<< HTML
        </div>
    </div>
</form>
HTML;
        }
    }
}


/**
 * Función que se encarga de mostrar la página de gestión de usuarios.
 * @param $usuarios array con la información de todos los usuarios de la base de datos.
 * @param $editar true en caso de que estemos editando el usuario false en caso contrario.
 */
function HTMLpag_gestionusuarios($editar, $usuarios=null, $admin=false, $datos=null){
    $exito = $error = '';
    if(isset($_SESSION['errormsg'])){
        $error = $_SESSION['errormsg'];
        unset($_SESSION['errormsg']);
    }

    if(isset($_SESSION['exitomsg'])){
        $exito = $_SESSION['exitomsg'];
        unset($_SESSION['exitomsg']);
    }

    echo <<< HTML
    <div class="contenidoCentral">
        <main class="contenidoIzquierda">
            <div class="gestionUsuariosTop">
HTML;
    echo "<h1>Gestión de usuarios</h1>";
    echo "<h2>Indique la acción a realizar</h2>";
    if($error != ''){
        echo "<h2 class='error'>$error</h2>";
    }

    if($exito != ''){
        echo "<h2 class='exito'>$exito</h2>";
    }
                if(!$editar){
                    echo <<< HTML
                    <div class="accionesUsuarios"> 
                    <nav>
                        <ul>
HTML;
echo "                <div id=\"accion\"><a href=\"".Config::BASE_URL."gestion/usuarios?listar=true"."\">Listado</a></div>";
                        echo <<< HTML

HTML;
echo "                <div id=\"accion\"><a href=\"".Config::BASE_URL."gestion/usuarios/create"."\">Añadir nuevo</a></div>";
                        echo <<< HTML
                        </ul>
                    </nav>
                </div>
HTML;
                }

                if(isset($_GET['listar'])){
                    HTMLlistar_usuarios($usuarios);
                }else{
                    if($admin){
                        HTMLnuevo_usuario($editar, true, $datos);
                    }else{
                        HTMLnuevo_usuario($editar, false, $datos);
                    }
                }


                echo <<< HTML
            </div>
        </main>
HTML;
}

