<?php


/**
 * Función que muestra la barra de navegación para un usuario sin identificar.
 */
function HTMLnav(){
echo <<< HTML
    <nav class="navInicio">
            <ul>
HTML;

echo "                <li><a href=\"".Config::BASE_URL."\">Ver Incidencias</a></li>";

echo <<< HTML
            </ul>
    </nav>
HTML;
}

/**
 * Función que muestra la barra de navegación para un usuario identificado como colaborador.
 */
function HTMLnavColaborador(){
    echo <<< HTML
        <nav class="navInicio">
                <ul>
HTML;
    
    echo "                <li><a href=\"".Config::BASE_URL."\">Ver Incidencias</a></li>";

    echo "                <li><a href=\"".Config::BASE_URL."incidencias/nueva\">Nueva Incidencia</a></li>";
    echo "                <li><a href=\"".Config::BASE_URL."incidencias/usuario\">Mis Incidencias</a></li>";
    
    echo <<< HTML
                </ul>
        </nav>
HTML;
}

/**
 * Función que muestra la barra de navegación para un usuario identificado como administrador.
 */
function HTMLnavAdministrador(){
    echo <<< HTML
        <nav class="navInicio">
                <ul>
HTML;

    echo "                <li><a href=\"".Config::BASE_URL."\">Ver Incidencias</a></li>";

    echo "                <li><a href=\"".Config::BASE_URL."incidencias/nueva\">Nueva Incidencia</a></li>";
    echo "                <li><a href=\"".Config::BASE_URL."incidencias/usuario\">Mis Incidencias</a></li>";
    echo "                <li><a href=\"".Config::BASE_URL."logs\">Ver Logs</a></li>";
    echo "                <li><a href=\"".Config::BASE_URL."gestion/bbdd\">Gestión BBDD</a></li>";
    echo "                <li><a href=\"".Config::BASE_URL."gestion/usuarios\">Gestión Usuarios</a></li>";
    
    
    echo <<< HTML
                </ul>
        </nav>
HTML;
}

?>