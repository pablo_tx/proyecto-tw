<?php

/** Funcion pàra mostrar formulario de creación de una nueva incidencia.
 * @param null $datos Datos de la incidencia en caso de que no sea null, será porque se está editando la incidencia
 */
function HTMLform_incidencia($datos=null){

    if(!isset($_GET['nueva_incidencia_exito'])) {
        $titulo = $lugar = $descripcion = $keywords = $keywordPlaceholder = $action = $required = $id_incidencia = '';

        if (isset($datos['titulo'])) $titulo = $datos['titulo'];
        if (isset($datos['descripcion'])) $descripcion = $datos['descripcion'];
        if (isset($datos['lugar'])) $lugar = $datos['lugar'];
        if (isset($datos['keywords'])) $keywords = $datos['keywords'];
        if (isset($datos['id'])) $id_incidencia = $datos['id'];

        (empty($datos)) ? $valorBoton = "Crear nueva incidencia" : $valorBoton = "Modificar Datos";
        (empty($datos)) ? $keywordPlaceholder = "Formato: clave1,clave2,clave3..." : $keywordPlaceholder = $keywords;

        if ($datos == null) {
            $required = "required";
            $action = "incidencias/create";
            echo <<< HTML
        <div class="tituloNuevaIncidencia">
            <h1>Nueva incidencia</h1>
        </div>
HTML;
        } else {
            $action = "incidencias/update";
        }
        echo <<< HTML


            <h2>Datos principales</h2>

            <div class="formularioContainer">
HTML;

        if(isset($_GET['titulo'])){
            $titulo = $_GET['titulo'];
        }

        if(isset($_GET['descripcion'])){
            $descripcion = $_GET['descripcion'];
        }

        if(isset($_GET['lugar'])){
            $lugar = $_GET['lugar'];
        }

        if(isset($_GET['keywords'])){
            $keywords = $_GET['keywords'];
        }

        echo "   <form action=\"" . Config::BASE_URL . "$action\"  method=\"post\">";
        echo <<< HTML
                    <div>
                        <label for="titulo"><b>Título</b></label>
                        <input type="text" placeholder="$titulo" name="titulo" value='$titulo' $required>
                        <input type="hidden" name="editid" value='$id_incidencia'>
                    </div>
                    <div>
                        <label for="descripcion"><b>Descripción</b></label>
                        <textarea rows="10" placeholder="$descripcion" name="descripcion" $required>$descripcion</textarea>
                    </div>
                    <div>
                        <label for="lugar"><b>Lugar</b></label>
                        <input type="text" placeholder="$lugar" value='$lugar' name="lugar" $required>
                    </div>
                    <div>
                        <label for="keywords"><b>Palabras clave</b></label>
                        <input type="text" name="keywords" value='$keywords' placeholder="$keywordPlaceholder" $required>
                    </div>
                    <button type="submit">$valorBoton</button>
                </form>
HTML;
        if (isset($_SESSION["errormsg"])) {
            $error = $_SESSION["errormsg"];
            echo "  <h3>$error</h3>";
            unset($_SESSION["errormsg"]);
        }
        echo <<< HTML
    </div>
HTML;
    }
}

/**
 * Función para generar una nueva incidencia. Invoca a la función principal para el formulario
 * de la incidencia HTMLform_incidencia.
 */
function HTMLpag_nuevaincidencia(){
    echo <<< HTML

    <div class="contenidoCentral">
        <main class="contenidoIzquierda">
HTML;
            HTMLform_incidencia();
            echo <<< HTML
        </main>
HTML;
}

?>