<?php

/**
 * Función que muestra los logs del sistema.
 */
function HTMLpag_logs($logs){
    echo"    <link rel=\"stylesheet\" href=\"" . Config::BASE_URL . "css/logs.css\">";
    echo <<< HTML
    <div class="contenidoCentral">
        <main class="contenidoIzquierda">
            <h1>Eventos del sistema</h1>
            <div class="bloqueLogs">
HTML;

    foreach ($logs as $log){
        echo"<div class=\"errorLog\">";
        echo"<p class=\"fechaLogs\">".$log['fecha']."</p>";
        echo"<span></span>";
        echo"<p class=\"textoLogs\">".$log['texto']."</p>";
        echo"</div>";
    }
    echo <<< HTML
            </div>
        </main>
HTML;
}

?>