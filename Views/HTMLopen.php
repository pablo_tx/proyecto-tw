<?php

/**
 * Función cabecera. Se utiliza como apertura del blocque de cada página.
 * Pertenec a la parte común
 * @param $page request param para cargar el css correspondiente a la página que se va a cargar.
 */
function HTMLopen($page){
echo <<< HTML
<!DOCTYPE html>

<html lang = "es" >

<head >
    <meta charset = "utf-8" >
    <title>Queja-T</title >
    <meta name = "description" content = "Proyecto TW" >
    <meta name="author" content="Víctor Moreno, Pablo Pozo">
    <meta name = "viewport" content = "width=device-width, initial-scale=1" >
HTML;

echo"    <link rel=\"stylesheet\" href=\"" . Config::BASE_URL . "css/".$page.".css\">";
echo"    <link rel=\"stylesheet\" href=\"" . Config::BASE_URL . "css/common.css\">";
echo "</head >";

echo "<body >";

}

?>
