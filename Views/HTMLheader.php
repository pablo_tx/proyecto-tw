<?php


/**
 * Función para el header. Muestra el encabezado de la página con la imagen de título.
 */
function HTMLheader()
{
echo <<< HTML
            <div id="parteSuperiorInicio">
                <header id="tituloInicio" role="header">
HTML;
                    echo "<img id='imagenTitulo' src=\"" . Config::BASE_URL . "img/titulo.png\" />";
echo <<< HTML
HTML;
        echo <<< HTML
                </header>
            </div>
HTML;
}

?>

