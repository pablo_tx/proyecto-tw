<?php

require_once "Core/abstractmodel.php";


/**
 * Class ModeloComentario. Clase que se encarga de la interacción con la base de datos relacionada con los Comentarios.
 */
class ModeloComentario extends AbstractModel
{
    /**
     * ModeloComentario constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Función que devuelve todos los comentarios de una incidencia.
     * @param $id_incidencia incidencia a la que extraerle los comentarios.
     * @return array comentarios de la incidencia.
     */
    public function getComentariosIncidencia($id_incidencia)
    {
        $statement = $this->db->prepare("SELECT com.id, com.texto, com.fecha, usu.nombre FROM Comentarios com LEFT OUTER JOIN Usuarios usu ON com.id_usuario = usu.id WHERE id_incidencia=:id_incidencia");
        $statement->execute(array(':id_incidencia'=>$id_incidencia));

        return $statement->fetchAll();
    }


    /**
     * Función para insertar un comentario
     * @param $id_incidencia id incidencia comentada
     * @param $id_usuario id del usuario que comenta la incidencia
     * @return $res si ha tenido éxito la inserción.
     */
    public function insertarComentario($id_incidencia, $id_usuario, $comentario)
    {
        $statement = $this->db->prepare("INSERT INTO Comentarios(fecha,texto,id_incidencia,id_usuario)
                                         VALUES(NOW(), :texto, :id_incidencia, :id_usuario)");
        $res = $statement->execute(array(':texto'=>$comentario,':id_incidencia'=>$id_incidencia,':id_usuario'=>$id_usuario ));

        return $res;
    }


    /**
     * Funcición para borrar un comentario de una incidencia.
     * @param $id_incidencia id de la incidencia que tiene el comentario
     * @param $id_usuario usuario que realizó el comentario.
     * @param $texto texto de el comentario
     * @return bool true si ha tenido éxito false en otro caso
     */
    public function borrarComentario($id_comentario)
    {
        $statement = $this->db->prepare("DELETE FROM Comentarios WHERE id=:id_comentario");
        return $statement->execute(array(':id_comentario'=>$id_comentario));
    }

    public function comentarioExists($id)
    {
        $statement = $this->db->prepare("SELECT * FROM Comentarios WHERE id=:id_comentario");
        $statement->execute(array(':id_comentario' => $id));
        $res = $statement->fetch();
        return !empty($res);
    }

    public function userIsOwner($id_comentario, $user_id)
    {
        $statement = $this->db->prepare("SELECT * FROM Comentarios WHERE id=:id_comentario AND id_usuario=:user_id");
        $statement->execute(array(':user_id' => $user_id, ':id_comentario' => $id_comentario));
        $res = $statement->fetch();
        return !empty($res);
    }


}


?>