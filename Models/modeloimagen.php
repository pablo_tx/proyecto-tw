<?php

require_once "Core/abstractmodel.php";


/**
 * Class ModeloImagen. Clase que se encarga de la interacción con la base de datos relacionada con las Imágenes.
 */
class ModeloImagen extends AbstractModel
{
    /**
     * ModeloImagen constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Función para obtener todas las imágenes de la incidencia $id_incidencia.
     * @param $id_incidencia id de la incidencia.
     * @return array imágenes de la incidencia $id_incidencia
     */
    public function getImagenesIncidencia($id_incidencia)
    {
        $statement = $this->db->prepare("SELECT imagen FROM Imagenes WHERE id_incidencia=:id_incidencia");
        $statement->execute(array(':id_incidencia'=>$id_incidencia));

        return $statement->fetchAll();
    }
}


?>