<?php

require_once "Core/abstractmodel.php";


/**
 * Class ModeloBackup. Clase que se encarga de la interacción con la base de datos relacionada con los Backup.
 */
class ModeloBackup extends AbstractModel
{

    /**
     * ModeloBackup constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Función para obtener todos los datos de la tabla $table.
     * @param $table tabla a la que se le extraen los datos.
     * @return bool|PDOStatement Si se ha realizado con éxito devolverá todos los datos de la tabla $table.
     */
    public function selectFromTable($table)
    {
        $statement = $this->db->prepare("SELECT * FROM ".$table);
        $statement->execute();

        return $statement;
    }

    /**
     * Función que muestra como crear la tabla $table.
     * @param $table tabla para mostrar la consulta para crearla
     * @return mixed
     */
    public function showCreateTable($table)
    {
        $statement = $this->db->prepare("SHOW CREATE TABLE ".$table);
        $statement->execute();

        return $statement->fetch();
    }

    /**
     * Función para establecer la comprobación de claves externas.
     * @param $status true si queremos hacer comprobación false en otro caso.
     */
    public function setFKCheck($status)
    {
        $status == false ? $status = 0 : $status = 1;
        $statement = $this->db->prepare("SET FOREIGN_KEY_CHECKS = ".$status);
        $statement->execute(array(':status' => $status));
    }

    /**
     * Función de vaciado de la tabla $table.
     * @param $table tabla a vaciar.
     */
    public function truncate($table)
    {
        $statement = $this->db->prepare("TRUNCATE TABLE ".$table);
        $statement->execute();
    }


    /**
     * Función que ejecuta la sentencia $sentence.
     * @param $sentence string con una sentence sql.
     * @return $res bool true si ha ido bien false en otro caso.
     */
    public function execute($sentence){
        $statement = $this->db->prepare($sentence);
        $res = $statement->execute();

        return $res;
    }
}

?>
