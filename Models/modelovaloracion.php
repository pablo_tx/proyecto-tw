<?php

require_once "Core/abstractmodel.php";

/**
 * Class ModeloValoracion
 * Modelo para el objeto Valoracion gestiona todo lo relacionado con la base de datos.
 *
 */
class ModeloValoracion extends AbstractModel
{
    /**
     * ModeloValoracion constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Método para obtener las valoraciones de una incidencia.
     * @param $id_incidencia incidencia para obtener las valoraciones
     * @return array las valoraciones de la incidencia.
     */
    public function getValoracionesIncidencia($id_incidencia)
    {
        $statement = $this->db->prepare("SELECT sum(case when valoracion=1 then 1 end) as positivas, sum(case when valoracion=0 then 1 end) as negativas FROM Valoraciones WHERE id_incidencia=:id_incidencia GROUP BY id_incidencia");

        $statement->execute(array(':id_incidencia'=>$id_incidencia));
        return $statement->fetchAll();
    }
}


?>