<?php

require_once "Core/abstractmodel.php";


/**
 * Class ModeloLogs. Clase que se encarga de la interacción con la base de datos relacionada con los Logs.
 */
class ModeloLogs extends AbstractModel
{
    /**
     * ModeloLogs constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Función para obtener todos los logs de la base de datos.
     * @return array todos los logs de la base de datos.
     */
    public function getAllLogs()
    {
        $statement = $this->db->prepare("SELECT * FROM Logs ORDER BY fecha DESC");
        $statement->execute();

        return $statement->fetchAll();
    }


    /**
     * Función para añadir una entrada a la tabla Logs.
     * @param $texto string información del Log.
     * @param $tipo int tipo de log.
     * @param $user string usuario que ha ejecutado la acción.
     * @return bool true si ha tenido éxito, false en otro caso.
     */
    public function addLog($texto, $tipo, $user)
    {
        $statement = $this->db->prepare("INSERT INTO Logs (tipo, fecha, texto, id_usuario) VALUES (:tipo, NOW(), :texto, :user)");
        $res = $statement->execute(array(':texto'=>$texto,':user'=>$user, ':tipo'=>$tipo));

        return $res;
    }
}


?>