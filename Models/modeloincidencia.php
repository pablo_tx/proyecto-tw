<?php

require_once "Core/abstractmodel.php";


/**
 * Class ModeloIncidencia. Clase que se encarga de la interacción con la base de datos relacionada con las Incidencias.
 */
class ModeloIncidencia extends AbstractModel
{
    /**
     * ModeloIncidencia constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Función que devuelve un array con todas las incidencias en el orden $orden.
     * @param $orden, orden para ordnear las incidencias, puede ser por antigüedad, o ordenada por valoraciones positivas.
     * @return array Incidencias ordenadas.
     */
    public function getAllIncidencias($orden)
    {

        $ordensel = '';
        switch ($orden) {
            case 'antiguedad':
                $ordensel = ' ORDER BY inc.fecha DESC';
                break;
            case 'positivosnetos':
                $ordensel = ' ORDER BY pnetos ASC';
                break;
            case 'positivos':
                $ordensel = ' ORDER BY positivos ASC';
                break;
        }

        $statement = $this->db->prepare('SELECT
                                                        val.positivos,
                                                        val.negativos,
                                                        val.pnetos,
                                                        inc.id,
                                                        inc.titulo,
                                                        inc.lugar,
                                                        inc.keywords,
                                                        inc.fecha,
                                                        inc.estado,
                                                        inc.descripcion,
                                                        usu.nombre,
                                                        usu.id as id_usuario
                                                    FROM
                                                        Incidencias inc
                                                    INNER JOIN Usuarios usu ON
                                                        inc.user_id = usu.id
                                                    LEFT OUTER JOIN (
                                                            SELECT id_incidencia,
                                                            COALESCE(sum(case when valoracion = 1 then 1 end),
                                                            0) as positivos,
                                                            COALESCE(sum(case when valoracion = 0 then 1 end),
                                                            0) as negativos,
                                                            CAST(COALESCE(sum(case when valoracion = 1 then 1 end),0) - COALESCE(sum(case when valoracion = 0 then 1 end),0) as signed) as pnetos
                                                        FROM
                                                            Valoraciones
                                                        GROUP BY
                                                            id_incidencia) val ON
                                                        inc.id = val.id_incidencia' .$ordensel);
        $statement->execute();


        return $statement->fetchAll();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getIncidencia($id)
    {
        $statement = $this->db->prepare("SELECT * FROM Incidencias WHERE id=:id");
        $statement->execute(array(':id' => $id));

        return $statement->fetch();
    }

    /**
     * Función que devuelve todas las incidencias creadas por el Usuario $idUsuario
     * @param $idUsuario id del usuario del cual se quieren obtener sus incidencias.
     * @return array Incidencias del usuario $idUsuario
     */
    public function getIncidenciasUsuario($idUsuario)
    {
        $statement = $this->db->prepare("SELECT inc.id, inc.titulo, inc.lugar, inc.keywords, inc.fecha, inc.estado, inc.descripcion, usu.nombre FROM Incidencias inc INNER JOIN Usuarios usu ON inc.user_id=usu.id WHERE inc.user_id=:idUsuario");
        $statement->execute(array(':idUsuario' => $idUsuario));

        return $statement->fetchAll();
    }

    /**
     * Función que devuelve las incidencias con un estado igual a $estado
     * @param $estado estado de las incidencias que se quieren obtener.
     * @return array Todas las incidencias con estado $estado.
     */
    public function getIncidenciasEstado($estado)
    {
        $statement = $this->db->prepare("SELECT * FROM Incidencias where user_id=:estado");
        $statement->execute(array(':estado' => $estado));

        return $statement->fetchAll();
    }

    /**
     * Función para borrar la incidencia con id incidencia $id.
     * @param $id int id incidencia a borrar.
     */
    public function borrarIncidencia($id)
    {
        $statement = $this->db->prepare("DELETE FROM Incidencias WHERE id=:id");
        return $statement->execute(array(':id' => $id));
    }

    /**
     * Función encargada de crear una incidencia.
     * @param $user_id int id del usuario creador de la incidencia.
     * @param $titulo string titulo de incidencia.
     * @param $descripcion string descripción de la incidencia.
     * @param $lugar string lugar de la incidencia.
     * @param $keywords string palabras clave de la incidencia.
     *
     * @return string|null En caso de éxito devuelve la id de la incidencia creada, null en otro caso.
     */
    public function crearIncidencia($user_id, $titulo, $descripcion, $lugar, $keywords)
    {
        $statement = $this->db->prepare("INSERT INTO Incidencias (titulo, lugar, keywords, fecha, estado, descripcion, user_id)
                                                   VALUES (:titulo, :lugar, :keywords, NOW(), 0, :descripcion, :user_id)");
        $res = $statement->execute(array(':titulo' => $titulo, ':lugar' => $lugar, ':keywords' => $keywords, ':descripcion' => $descripcion, ':user_id' => $user_id));
        return $res ? $this->db->lastInsertId() : null;
    }

    /**
     * Función que se encarga de moficiar una Incidencia ya creada.
     * @param $id_incidencia int id de la incidencia a modificar.
     * @param $titulo string titulo de incidencia.
     * @param $descripcion string descripción de la incidencia.
     * @param $lugar string lugar de la incidencia.
     * @param $keywords string palabras clave de la incidencia.
     */
    public function actualizarincidencia($id_incidencia, $titulo, $descripcion, $lugar, $keywords)
    {
        $statement = $this->db->prepare("UPDATE Incidencias SET titulo=:titulo, descripcion=:descripcion, lugar=:lugar, keywords=:keywords WHERE id=:id_incidencia");
        return $statement->execute(array(':titulo' => $titulo, ':descripcion' => $descripcion, ':lugar' => $lugar, ':keywords' => $keywords, ':id_incidencia' => $id_incidencia));
    }


    /**
     * Función para actualizar el estado de la incidencia con id incidencia $id_incidencia.
     * @param $id_incidencia int id incidencia de la incidencia que se va a actualizar
     * @param $estado string nuevo estado de la incidencia.
     * @return bool true en caso de éxito false en otro caso.
     */
    public function actualizarEstadoincidencia($id_incidencia, $estado)
    {
        $nuevo_estado='';
        switch ($estado){
            case 'Pendiente':
                $nuevo_estado = 0;
                break;
            case 'Comprobada':
                $nuevo_estado = 1;
                break;
            case 'Tramitada':
                $nuevo_estado = 2;
                break;
            case 'Irresoluble':
                $nuevo_estado = 3;
                break;
            case 'Resuelta':
                $nuevo_estado = 4;
                break;

        }
        $statement = $this->db->prepare("UPDATE Incidencias SET estado=:estado WHERE id=:id_incidencia");
        $res = $statement->execute(array(':estado' => $nuevo_estado, ':id_incidencia' => $id_incidencia));

        return $res == null ? false : true;

    }

    /**
     * Función para modificar el estado de la incidencia $id_incidencia
     * @param $id_incidencia int id de la incidencia a modificarle el estado.
     * @param $estado string nuevo estado para la incidencia $id_incidencia.
     * @return bool true en caso de éxito false en otro caso.
     */
    public function setEstadoIncidencia($id_incidencia, $estado)
    {
        $statement = $this->db->prepare("UPDATE Incidencias SET estado=:estado WHERE id=:id_incidencia");
        $res = $statement->execute(array(':estado' => $estado, ':id_incidencia' => $id_incidencia));

        return $res == null ? false : true;
    }

    /**
     * Función para actualizar el título de la incidencia $id_incidencia.
     * @param $id_incidencia int id de la incidencia a la que se le va a actualizar el titulo.
     * @param $titulo string nuevo titulo para la incidencia.
     * @return bool true en caso de éxito false en otro caso.
     */
    public function setTitulo($id_incidencia, $titulo)
    {
        $statement = $this->db->prepare("UPDATE Incidencias SET titulo=:titulo WHERE id=:id_incidencia");
        $res = $statement->execute(array(':titulo' => $titulo, ':id_incidencia' => $id_incidencia));

        return $res == null ? false : true;
    }

    /**
     * Función para modificar la descripcion de la incidencia $id_incidencia.
     * @param $id_incidencia id de la incidencia que se va a modificar.
     * @param $descripcion nueva descripción para la incidencia.
     * @return bool true en caso de éxito false en otro caso.
     */
    public function setDescripcion($id_incidencia, $descripcion)
    {
        $statement = $this->db->prepare("UPDATE Incidencias SET descripcion=:descripcion WHERE id=:id_incidencia");
        $res = $statement->execute(array(':descripcion' => $descripcion, ':id_incidencia' => $id_incidencia));

        return $res == null ? false : true;
    }

    /**
     * Función para modificar el lugar de la incidencia $id_incidencia
     * @param $id_incidencia id de la incidencia a modificar.
     * @param $lugar nuevo lugar para la incidencia
     * @return bool true en caso de éxito false en otro caso.
     */
    public function setLugar($id_incidencia, $lugar)
    {
        $statement = $this->db->prepare("UPDATE Incidencias SET lugar=:lugar WHERE id=:id_incidencia");
        $res = $statement->execute(array(':lugar' => $lugar, ':id_incidencia' => $id_incidencia));

        return $res == null ? false : true;
    }

    /**
     * Función para modificar las palabras clave o keywords de la incidencia $id_incidencia
     * @param $id_incidencia id de la incidencia a modificar
     * @param $keywords nuevas palabras clave
     */
    public function setKeyWords($id_incidencia, $keywords)
    {
        $statement = $this->db->prepare("UPDATE Incidencias SET keywords=:keywords WHERE id=:id_incidencia");
        $statement->execute(array(':keywords' => $keywords, ':id_incidencia' => $id_incidencia));
    }

    /**
     * Función para saneamiento de cadenas-
     * @param $string String de entrada para ser saneado.
     * @return string String saneado.
     */
    public function sanitizeString($string)
    {
        $string = strip_tags($string);
        // Comentar para conservar caracteres especiales
        //$string = preg_replace('/\PL/u', '', $string);
        return $string;
    }

    /**
     * Función que comprueba si el usuario con $user_id ha creado la incidencia $incidencia_id.
     * @param $incidencia_id id de la incidencia a comprobar si $user_id es propietario.
     * @param $user_id usuario a comprobar si $incidencia_id ha sido creada por el.
     * @return bool true si ha tenido éxito false en otro caso
     */
    public function userIsOwner($incidencia_id, $user_id)
    {
        $statement = $this->db->prepare("SELECT * FROM Incidencias WHERE id=:incidencia_id AND user_id=:user_id");
        $statement->execute(array(':user_id' => $user_id, ':incidencia_id' => $incidencia_id));
        $res = $statement->fetch();
        return !empty($res);
    }

    /**
     * Función para añadir una imagen a la incidencia $id_incidencia
     * @param $id_incidencia int id de la incidencia a la cual se quiere añadir una imagen.
     * @param $imagen string imagen que se quiere añadir a la incidencia.
     * @return string|null En caso de éxito devuelve el id de la imagen insertada, null en otro caso.
     */
    public function addImage($id_incidencia, $imagen)
    {
        $statement = $this->db->prepare("INSERT INTO Imagenes (id_incidencia, imagen)
                                                   VALUES (:id_incidencia, :imagen)");
        $res = $statement->execute(array(':id_incidencia' => $id_incidencia, ':imagen' => $imagen));
        return $res ? $this->db->lastInsertId() : null;
    }

    /**
     * Función para votar una incidencia. Se encarga de aumentar o disminuir los votos.
     * @param $id_incidencia id de la incidencia que se está votando.
     * @param $id_usuario usuario que está votando.
     * @param $voto voto negativo o positivo para la incidencia.
     * @return bool true en caso de éxito false en otro caso.
     */
    public function votar($id_incidencia, $id_usuario, $voto)
    {
        $statement = $this->db->prepare("INSERT INTO Valoraciones(id_incidencia, id_usuario, valoracion)
                                                  VALUES (:id_incidencia, :id_usuario, :voto ) ON DUPLICATE KEY UPDATE valoracion=:voto");

        $res = $statement->execute(array(':id_incidencia' => $id_incidencia, ':id_usuario' => $id_usuario, ':voto' => $voto));

        return $res;
    }

    /**
     * Función para comprobar si la incidencia con $id_incidencia existe.
     * @param int $id_incidencia id de la incidencia a ser comprobada.
     * @return bool true si existe false en otro caso.
     */
    public function incidenciaExists($id_incidencia)
    {
        $statement = $this->db->prepare("SELECT * FROM Incidencias WHERE id=:incidencia_id");
        $statement->execute(array(':incidencia_id' => $id_incidencia));
        $res = $statement->fetch();
        return !empty($res);
    }


    /**
     * Función para obtener las incidencias con el filtro de búsqueda.
     * @param $orden orden en que se quieren obtener las incidencias.
     * @param $textoBusqueda texto de búsqueda para hacer match en título o descripción.
     * @param $lugar texto de búsqueda para hacer match en lugar
     * @param $estado estado de la incidencia para filtrar.
     * @return array las incidencias ordenadas y filtradas.
     */
    public function getIncidenciasFiltradas($orden, $textoBusqueda, $lugar, $estado)
    {
        $estadoint=-1;
        switch ($estado){
            case 'Pendiente':
                $estadoint = 0;
                break;
            case 'Comprobada':
                $estadoint = 1;
                break;
            case 'Tramitada':
                $estadoint = 2;
                break;
            case 'Irresoluble':
                $estadoint = 3;
                break;
            case 'Resuelta':
                $estadoint = 4;
                break;
        }

        $ordensel = '';
        switch ($orden) {
            case 'antiguedad':
                $ordensel = ' ORDER BY inc.fecha DESC';
                break;
            case 'positivosnetos':
                $ordensel = ' ORDER BY pnetos DESC';
                break;
            case 'positivos':
                $ordensel = ' ORDER BY positivos DESC';
                break;
        }
        $countsel = 0;
        $arrayparams = array();

        $operator = 'WHERE';

        if (empty($textoBusqueda)) {
            $textosel = '';
        } else {
            $countsel++;
            $arrayparams[':texto'] = $textoBusqueda;
            $textosel = ' '.$operator.' (inc.titulo LIKE CONCAT(\'%\',:texto,\'%\') OR inc.descripcion LIKE CONCAT(\'%\',:texto,\'%\') OR inc.keywords LIKE CONCAT(\'%\',:texto,\'%\'))';
        }

        if (empty($lugar)) {
            $lugarsel = '';
        } else {
            $countsel++;
            $arrayparams[':lugar'] = $lugar;

            if($countsel > 1){ $operator = 'AND'; }
            $lugarsel = ' '.$operator.' inc.lugar LIKE CONCAT(\'%\',:lugar,\'%\')';
        }

        if (empty($estado)) {
            $estadosel = '';
        } else {
            $countsel++;
            $arrayparams[':estado'] = $estadoint;
            if($countsel > 1){ $operator = 'AND'; }
            $estadosel = ' '.$operator.' inc.estado = :estado';
        }


        $statement = $this->db->prepare('SELECT
                                                        val.positivos,
                                                        val.negativos,
                                                        val.pnetos,
                                                        inc.id,
                                                        inc.titulo,
                                                        inc.lugar,
                                                        inc.keywords,
                                                        inc.fecha,
                                                        inc.estado,
                                                        inc.descripcion,
                                                        usu.nombre,
                                                        usu.id as id_usuario
                                                    FROM
                                                        Incidencias inc
                                                    INNER JOIN Usuarios usu ON
                                                        inc.user_id = usu.id
                                                    LEFT OUTER JOIN (
                                                            SELECT id_incidencia,
                                                            COALESCE(sum(case when valoracion = 1 then 1 end),
                                                            0) as positivos,
                                                            COALESCE(sum(case when valoracion = 0 then 1 end),
                                                            0) as negativos,
                                                            CAST(COALESCE(sum(case when valoracion = 1 then 1 end),0) - COALESCE(sum(case when valoracion = 0 then 1 end),0) as signed) as pnetos
                                                        FROM
                                                            Valoraciones
                                                        GROUP BY
                                                            id_incidencia) val ON
                                                        inc.id = val.id_incidencia' .$textosel.$lugarsel.$estadosel.$ordensel);
        $statement->execute($arrayparams);

        return $statement->fetchAll();
    }

}
