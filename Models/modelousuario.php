<?php

require_once "Core/abstractmodel.php";


/**
 * Class ModeloUsuario
 */
class ModeloUsuario extends AbstractModel
{
    /**
     * ModeloUsuario constructor.
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->adminExists()) {
            $this->createAdmin();
        }
    }

    /**
     * @param $nombre
     * @param $apellidos
     * @param $email
     * @param $password
     * @param $tipo
     * @param $direccion
     * @param $telefono
     * @param $imagen
     * @return string|null
     */
    public function createUser($nombre, $apellidos, $email, $password, $tipo, $direccion, $telefono, $imagen, $estado)
    {
        $pass = password_hash($password, PASSWORD_DEFAULT);
        $statement = $this->db->prepare("INSERT INTO Usuarios (nombre, apellidos, email, password, tipo, direccion, telefono, imagen, estado) 
                                                       VALUES (:nombre,:apellidos,:email, :pass, :tipo, :direccion, :telefono, :imagen, :estado)");

        $res =  $statement->execute(array(':pass' => $pass,':nombre' => $nombre,':apellidos' => $apellidos,':email' => $email,
                                  ':tipo' => $tipo,':direccion' => $direccion,':telefono' => $telefono, ':imagen' => $imagen, ':estado' => $estado));

        return $res ? $this->db->lastInsertId() : null;
    }

    /**
     * @param $estado
     * @param $user_id
     */
    public function setUserState($estado, $user_id)
    {
        $statement = $this->db->prepare("UPDATE Usuarios SET estado=:estado WHERE id=:user_id");
        $statement->execute(array(':estado' => $estado,':user_id' => $user_id));
    }

    /**
     *
     */
    public function createAdmin()
    {
            $pass = password_hash("admin", PASSWORD_DEFAULT);
            $statement = $this->db->prepare("INSERT INTO Usuarios (nombre, apellidos, email, password, tipo, direccion, telefono, imagen, estado) 
                                                       VALUES ('admin','admin','admin@admin.com', :pass, 1,'lacasadeladmin','000000000', 'Views/img/profile.png', 1)");
            $statement->execute(array(':pass' => $pass));
    }

    /**
     * @return bool
     */
    public function adminExists(){
        $statement = $this->db->prepare("SELECT count(nombre) as NUM FROM Usuarios WHERE nombre='admin'");
        $statement->execute();
        $result = $statement->fetch();
        return $result['NUM'] != 0 ? true : false;
    }

    /**
     * Función que comprueba si existe el usuario con nombre $nombre.
     * @return bool true si existe el usuario $nombre false en otro caso
     */
    public function existeUsuario($nombre){
        $statement = $this->db->prepare("SELECT nombre FROM Usuarios where nombre=:nombre");
        $statement->execute(array(':nombre' => $nombre));
        $result = $statement->fetch();
        if(empty($result)){
            return false;
        }
        return true;
    }

    /**
     * Función que comprueba si existe el usuario con mail $mail.
     * @return bool true si existe el usuario $mail false en otro caso
     */
    public function existeUsuarioMail($mail){
        $statement = $this->db->prepare("SELECT nombre FROM Usuarios where email=:email");
        $statement->execute(array(':email' => $mail));
        $result = $statement->fetch();
        if(empty($result)){
            return false;
        }
        return true;
    }

    /**
     * Función que comprueba si existe el usuario con nombre $telefono.
     * @return bool true si existe el usuario $telefono false en otro caso
     */
    public function existeUsuarioTelefono($telefono){
        $statement = $this->db->prepare("SELECT nombre FROM Usuarios where telefono=:telefono");
        $statement->execute(array(':telefono' => $telefono));
        $result = $statement->fetch();
        if(empty($result)){
            return false;
        }
        return true;
    }

    /**
     * @param $user
     * @param $pass
     * @return array|null
     */
    public function checkCredentials($user, $pass){
        $statement = $this->db->prepare("SELECT nombre, password, tipo, estado, id FROM Usuarios WHERE nombre=:nombre");
        $statement->execute(array(':nombre' => $user));
        $result = $statement->fetch();

        if (password_verify($pass, $result['password'])){
            return array('tipo' => $result['tipo'], 'id' => $result['id'], 'estado' => $result['estado']);
        }else{
            return null;
        }
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getProfileImage($user_id)
    {
        $statement = $this->db->prepare("SELECT imagen FROM Usuarios WHERE id=:user_id");
        $statement->execute(array(':user_id' => $user_id));
        $result = $statement->fetch();
        return $result['imagen'];
    }

    /**
     * @return array
     */
    public function getAllUsuarios()
    {
        $statement = $this->db->prepare("SELECT * FROM Usuarios");
        $statement->execute();

        return $statement->fetchAll();
    }


    /**
     * Función para borrar el usuario con el mail $mail
     * @param $mail email del usuario que vamos a borrar.
     * @return $res, si se ha realizado la consulta con éxito.
     */
    public function deleteUser($mail){
        $statement = $this->db->prepare("DELETE FROM Usuarios WHERE email=:email");
        $res = $statement->execute(array(':email' => $mail));

        return $res;
    }


    /**
     * Función para obtener el id del usuario a través de su nombre.
     * @param $nombre Nombre del usuario
     * @return array
     */
    public function getUserId($nombre){
        $statement = $this->db->prepare("SELECT id FROM Usuarios WHERE nombre=:nombre");
        $statement->execute(array(':nombre' => $nombre));

        return $statement->fetchAll();
    }

    /**
     * Función para obtener el id del usuario a través de su email.
     * @param $email string con el email del usuario
     * @return array el id del usuario
     */
    public function getUserIdMail($email){
        $statement = $this->db->prepare("SELECT id FROM Usuarios WHERE email=:email");
        $statement->execute(array(':email' => $email));

        return $statement->fetchAll();
    }

    /**
     * Función que devuelve el nombre de usuario que tiene la id $id_usuario
     * @param $id_usuario id del usuario
     * @return array el nombre del usuario con $id_usuario.
     */
    public function getNombreUsuario($id_usuario){
        $statement = $this->db->prepare("SELECT nombre FROM Usuarios WHERE id=:id");
        $statement->execute(array(':id' => $id_usuario));

        return $statement->fetchAll();
    }


    /**
     * Función para obtener todos los datos del usuario con id $id_usuario
     * @param  int $id_usuario id del usuario a obtener los datos
     * @return array todos los datos del usuario.
     */
    public function getAllData($id_usuario){
        $statement = $this->db->prepare("SELECT * FROM Usuarios WHERE id=:id");
        $statement->execute(array(':id' => $id_usuario));

        return $statement->fetchAll();
    }


    /**
     * Función que devuelve los 3 usuarios que más comentarios realizan.
     * @return array con los nombres de los usuarios que más comentarios realizan.
     */
    public function getlosQueMasComentan(){

        $statement = $this->db->prepare("SELECT nombre,count(*) as n FROM Usuarios INNER JOIN Comentarios ON Usuarios.id=Comentarios.id_usuario GROUP BY nombre ORDER BY n DESC LIMIT 3");
        $statement->execute();

        return $statement->fetchAll();
    }

    /**
     * Función que devuelve los 3 usuarios que más incidencias aniaden.
     * @return array con los nombres de los usuarios que más incidencias realizan.
     */
    public function getlosqueMasIncidencias(){

        $statement = $this->db->prepare("SELECT nombre,count(*) as n FROM Usuarios INNER JOIN Incidencias ON Usuarios.id=Incidencias.user_id GROUP BY nombre ORDER BY n DESC LIMIT 3");
        $statement->execute();

        return $statement->fetchAll();
    }

    public function usuarioExists($id)
    {
        $statement = $this->db->prepare("SELECT * FROM Usuarios WHERE id=:user_id");
        $statement->execute(array(':user_id' => $id));
        $res = $statement->fetch();
        return !empty($res);
    }

    public function updateUser($id, $nombre, $apellidos, $email, $passwd, $rol, $direccion, $tlf, $target_file, $estado)
    {
        $pass = password_hash($passwd, PASSWORD_DEFAULT);
        $statement = $this->db->prepare("UPDATE Usuarios SET nombre=:nombre,apellidos=:apellidos,email=:email, password=:pass, tipo=:tipo, direccion=:direccion, telefono=:telefono, imagen=:imagen, estado=:estado WHERE id=:id");

        $res =  $statement->execute(array(':id' => $id,':pass' => $pass,':nombre' => $nombre,':apellidos' => $apellidos,':email' => $email,
            ':tipo' => $rol,':direccion' => $direccion,':telefono' => $tlf, ':imagen' => $target_file, ':estado' => $estado));
        return $res;
    }

    public function updateUserData($id, $nombre, $apellidos, $email, $passwd, $rol, $direccion, $tlf, $estado)
    {
        $pass = password_hash($passwd, PASSWORD_DEFAULT);
        $statement = $this->db->prepare("UPDATE Usuarios SET nombre=:nombre,apellidos=:apellidos,email=:email, password=:pass, tipo=:tipo, direccion=:direccion, telefono=:telefono, estado=:estado WHERE id=:id");

        $res =  $statement->execute(array(':id' => $id,':pass' => $pass,':nombre' => $nombre,':apellidos' => $apellidos,':email' => $email,
            ':tipo' => $rol,':direccion' => $direccion,':telefono' => $tlf, ':estado' => $estado));
        return $res;
    }


}

?>