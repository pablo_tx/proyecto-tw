SET FOREIGN_KEY_CHECKS=0;
DROP TABLE Usuarios;

CREATE TABLE `Usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `imagen` blob,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO Usuarios VALUES("1","admin","admin","admin@admin.com","$2y$10$zYggGqW4NN0K6vIBq9Vk1.DZASqZAeuD/60kBs3f3ZwAv5YcKWxG6","1","lacasadeladmin","654678654","Views/img/profile.png","1");
INSERT INTO Usuarios VALUES("4","Pablo","Pozo","pozotena@mail.com","$2y$10$MoqVfMnOS/90S4idEbgSqeiZRCaayoseAW4.bKzjwl7QBIyYyHJTe","0","dirección","765876987","Views/img/usuarios/Pablo.png","1");
INSERT INTO Usuarios VALUES("5","Victor","Moreno","victormoreno@mail.com","$2y$10$n50LXahn5V/vr.Qpt5DMMOT3No6b2jjJDqZrzJwJGnwPsQluy0/Le","0","dirección","678987789","Views/img/usuarios/Victor.png","1");
INSERT INTO Usuarios VALUES("6","Rosa","Koldo","rosakoldo@mail.com","$2y$10$D3JvqsHdMDN/DmoGRe9SQOGZPBQbTpKvNv0ED0i3nj2WrdCCcPMne","0","dirección Rosa Koldo","786543456","Views/img/usuarios/Rosa.png","0");
INSERT INTO Usuarios VALUES("7","Iluminada","DeDios","iluminada@gmail.com","$2y$10$78OKkJwyrb6oTfEFePfC8e3PNvmE1zCPm8uFTMUcjZnOL.j19Onvq","0","dirección","654321987","Views/img/usuarios/Iluminada.png","1");
INSERT INTO Usuarios VALUES("8","profesor","profesor","profesor@mail.com","$2y$10$WjVf.96DNXGOslTnQSR26uZx2uiUIhxIMRfNM7RQYiQ3s4YwOx2Zq","1","dirección","789890432","Views/img/usuarios/profesor.png","1");
INSERT INTO Usuarios VALUES("9","Jose","Herrero","jose@mail.com","$2y$10$0GT4GLKCAo6CUDzP2FnPouTbBcPO4hevxYcxV9qyXgF7YcbqylhzW","0","dirección Jose","876234987","Views/img/usuarios/Jose.png","0");


DROP TABLE Incidencias;

CREATE TABLE `Incidencias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lugar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` int(11) NOT NULL,
  `descripcion` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Incidencias_fk0` (`user_id`),
  CONSTRAINT `Incidencias_fk0` FOREIGN KEY (`user_id`) REFERENCES `Usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO Incidencias VALUES("1","Apagones en el barrio","Barrio la Encina","apagones,encima,calvario","2019-06-03 22:46:20","0","Hay apagones contínuos en el barrio todas las noches. Que alguien haga algo por favor","1");
INSERT INTO Incidencias VALUES("2","Retrovisor Roto","calle Gazpato 2","gazpato,retrovisor,vandalos","2019-06-03 22:52:55","1","Me han roto el retrovisor cerca de mi casa. Esto es un atropello. En 20 años en la comunidad jamás me había pasado algo parecido. Hay que hacer algo con estos vándalos.","1");
INSERT INTO Incidencias VALUES("3","Huelga de estudiantes","calle Kamunias","huelga,ruido,molesto","2019-06-03 23:00:15","0","Se ha convocado una huelga en la puerta de mi casa! No se que quieren pero es muy molesto. Hacen mucho ruido.","8");
INSERT INTO Incidencias VALUES("4","El perro de mi vecino ladra mucho","calle Ciruela 27","vecino,perro,ruido","2019-06-03 23:02:18","4","El perro de mi vecino, no para de ladrar durante el día y la noche. No hay quien duerma y mis niños no pueden estudiar. Hay que hacer algo o llamaré a la policía","8");
INSERT INTO Incidencias VALUES("5","Graffiti en la puerta de el edificio","calle Ciruela 27","grafiti,pintada,artista","2019-06-03 23:05:29","4","HAn pintado unos grafitis en la puerta de el edificio de la comunidad. No se quien habrá sido pero me gustan! Enhorabuena a los artistas y que sigan pintando, que llegarán lejos! Si se pueden poner en contacto conmigo me gustaría que me pintarán el salón.","8");
INSERT INTO Incidencias VALUES("8","Árbol caído","plaza mayor","arbol,plaza,urbanizacion","2019-06-03 23:25:24","0","Se ha caído un árbol en la plaza mayor de la urbanización. No se como se ha de proceder en estos casos, alguien sabe algo? Gracias de antemano","6");
INSERT INTO Incidencias VALUES("9","Deposiciones de los perros en nuestras aceras","calle Bobadilla","perro,excrementos,bobadilla","2019-06-04 11:12:55","0","Cada vez es más frecuente encontrar deposiciones de perros en nuestras aceras. Se ruega a los propietarios de los animales que recojan las deposiciones de sus perros. Por favor","9");
INSERT INTO Incidencias VALUES("10","Música muy alta a partir de las 00.00","Urbanización el Lazarillo","lazarillo,ruido,musica","2019-06-04 11:16:22","0","No se quien es pero está poniendo una música muy alta en horas  no decentes. Por favor se ruega que se cese esta actividad ya que me está perjudicando a la salud.","9");
INSERT INTO Incidencias VALUES("11","Gotera en el 4ºB","calle manzanares 5","manzanares,gotera","2019-06-04 11:21:31","0","Desde anoche tengo una gotera en el salón de mi piso. He intentado comunicarme con el piso de arriba pero nadie me contesta, alguien sabe algo? Me estoy empezando a preocupar.","7");
INSERT INTO Incidencias VALUES("12","Obras!","calle Rosal 42","obras,polvo,ruido","2019-06-04 11:27:48","0","En mi calle están de obras y es muy desagradable. Hace mucho ruido y se levanta mucho polvo... Estoy cansada de las obras, se puede hacer algo?","6");
INSERT INTO Incidencias VALUES("13","Cambio de ponderación de practicas","La facultad","practicas,inyustisia","2019-06-04 16:30:15","3","El profesor de la asignatura ha cambiado la ponderación de la practica de PHP, justamente en la practica que menos nota hemos sacado.\n\n\n\nNo me parece justo que le suban la nota, tendría que valer lo mismo que las practicas de HTML y CSS que se hacen en 5 minutos.","7");


DROP TABLE Valoraciones;

CREATE TABLE `Valoraciones` (
  `id_incidencia` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `valoracion` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_incidencia`,`id_usuario`),
  CONSTRAINT `Valoraciones_fk0` FOREIGN KEY (`id_incidencia`) REFERENCES `Incidencias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO Valoraciones VALUES("1","5","0");
INSERT INTO Valoraciones VALUES("3","5","1");
INSERT INTO Valoraciones VALUES("8","6","1");
INSERT INTO Valoraciones VALUES("9","5","0");
INSERT INTO Valoraciones VALUES("10","5","1");
INSERT INTO Valoraciones VALUES("10","821021782","1");
INSERT INTO Valoraciones VALUES("11","5","1");
INSERT INTO Valoraciones VALUES("11","821021782","1");
INSERT INTO Valoraciones VALUES("12","5","0");
INSERT INTO Valoraciones VALUES("12","821021782","1");
INSERT INTO Valoraciones VALUES("13","1","1");
INSERT INTO Valoraciones VALUES("13","4","1");
INSERT INTO Valoraciones VALUES("13","5","1");
INSERT INTO Valoraciones VALUES("13","821021782","0");


DROP TABLE Logs;

CREATE TABLE `Logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `texto` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO Logs VALUES("3","2","2019-06-03 22:19:13","Se ha creado el usuario Pablo","4");
INSERT INTO Logs VALUES("4","2","2019-06-03 22:20:02","Se ha creado el usuario Victor","5");
INSERT INTO Logs VALUES("5","2","2019-06-03 22:20:47","Se ha creado el usuario Rosa","6");
INSERT INTO Logs VALUES("6","2","2019-06-03 22:21:43","Se ha creado el usuario Iluminada","7");
INSERT INTO Logs VALUES("7","9","2019-06-03 22:46:20","Usuario admin ha creado la incidencia Apagones en el barrio con id 1","1");
INSERT INTO Logs VALUES("8","10","2019-06-03 22:46:24","Usuario admin ha subido una imagen (Views/img/incidencias/apagones.jpg) a la incidencia 1","1");
INSERT INTO Logs VALUES("9","12","2019-06-03 22:51:19","El usuario 1 ha creado un comentario","1");
INSERT INTO Logs VALUES("10","9","2019-06-03 22:52:55","Usuario admin ha creado la incidencia Retrovisor Roto con id 2","1");
INSERT INTO Logs VALUES("11","10","2019-06-03 22:53:02","Usuario admin ha subido una imagen (Views/img/incidencias/retrovisorroto.jpg) a la incidencia 2","1");
INSERT INTO Logs VALUES("12","8","2019-06-03 22:53:08","Usuario admin ha cambiado el estado de la incidencia 2 a Comprobada","1");
INSERT INTO Logs VALUES("13","12","2019-06-03 22:53:28","El usuario 1 ha creado un comentario","1");
INSERT INTO Logs VALUES("14","1","2019-06-03 22:53:32","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("103","10","2019-06-04 11:38:54","Usuario admin ha subido una imagen (Views/img/incidencias/apagones.jpg) a la incidencia 1","1");
INSERT INTO Logs VALUES("104","10","2019-06-04 11:39:06","Usuario admin ha subido una imagen (Views/img/incidencias/obras.jpg) a la incidencia 12","1");
INSERT INTO Logs VALUES("105","0","2019-06-04 11:51:06","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("106","10","2019-06-04 11:52:17","Usuario admin ha subido una imagen (Views/img/incidencias/gotera.JPG) a la incidencia 11","1");
INSERT INTO Logs VALUES("107","10","2019-06-04 11:52:49","Usuario admin ha subido una imagen (Views/img/incidencias/mierdaperro.jpg) a la incidencia 9","1");
INSERT INTO Logs VALUES("108","10","2019-06-04 11:54:45","Usuario admin ha subido una imagen (Views/img/incidencias/huelga.jpg) a la incidencia 3","1");
INSERT INTO Logs VALUES("109","12","2019-06-04 11:54:56","El usuario 1 ha creado un comentario","1");
INSERT INTO Logs VALUES("110","10","2019-06-04 11:55:12","Usuario admin ha subido una imagen (Views/img/incidencias/arbolcaido.jpg) a la incidencia 8","1");
INSERT INTO Logs VALUES("111","12","2019-06-04 11:55:24","El usuario 1 ha creado un comentario","1");
INSERT INTO Logs VALUES("112","12","2019-06-04 11:55:31","El usuario 1 ha creado un comentario","1");
INSERT INTO Logs VALUES("113","1","2019-06-04 11:56:53","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("114","0","2019-06-04 11:56:56","Usuario Rosa ha iniciado sesión","6");
INSERT INTO Logs VALUES("115","12","2019-06-04 11:57:05","El usuario 6 ha creado un comentario","6");
INSERT INTO Logs VALUES("116","12","2019-06-04 11:57:20","El usuario 6 ha creado un comentario","6");
INSERT INTO Logs VALUES("117","12","2019-06-04 11:57:37","El usuario 6 ha creado un comentario","6");
INSERT INTO Logs VALUES("118","12","2019-06-04 11:58:15","El usuario 6 ha creado un comentario","6");
INSERT INTO Logs VALUES("119","12","2019-06-04 11:58:26","El usuario 6 ha creado un comentario","6");
INSERT INTO Logs VALUES("120","1","2019-06-04 11:58:28","Usuario Rosa ha cerrado sesión","6");
INSERT INTO Logs VALUES("121","0","2019-06-04 11:58:31","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("122","10","2019-06-04 11:59:01","Usuario admin ha subido una imagen (Views/img/incidencias/grafiti.jpg) a la incidencia 5","1");
INSERT INTO Logs VALUES("123","10","2019-06-04 11:59:38","Usuario admin ha subido una imagen (Views/img/incidencias/retrovisorroto.jpg) a la incidencia 2","1");
INSERT INTO Logs VALUES("124","10","2019-06-04 11:59:56","Usuario admin ha subido una imagen (Views/img/incidencias/ladridoperro.png) a la incidencia 4","1");
INSERT INTO Logs VALUES("125","1","2019-06-04 12:06:43","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("126","0","2019-06-04 12:06:49","Usuario Iluminada ha iniciado sesión","7");
INSERT INTO Logs VALUES("127","12","2019-06-04 12:07:02","El usuario 7 ha creado un comentario","7");
INSERT INTO Logs VALUES("128","12","2019-06-04 12:07:35","El usuario 7 ha creado un comentario","7");
INSERT INTO Logs VALUES("129","12","2019-06-04 12:07:47","El usuario 7 ha creado un comentario","7");
INSERT INTO Logs VALUES("130","12","2019-06-04 12:08:01","El usuario 7 ha creado un comentario","7");
INSERT INTO Logs VALUES("131","12","2019-06-04 12:08:20","El usuario 7 ha creado un comentario","7");
INSERT INTO Logs VALUES("132","12","2019-06-04 12:08:58","El usuario 7 ha creado un comentario","7");
INSERT INTO Logs VALUES("133","12","2019-06-04 12:09:20","El usuario 7 ha creado un comentario","7");
INSERT INTO Logs VALUES("134","12","2019-06-04 12:10:53","El usuario 7 ha creado un comentario","7");
INSERT INTO Logs VALUES("135","1","2019-06-04 12:15:58","Usuario Iluminada ha cerrado sesión","7");
INSERT INTO Logs VALUES("136","0","2019-06-04 12:16:05","Usuario profesor ha iniciado sesión","8");
INSERT INTO Logs VALUES("137","1","2019-06-04 16:58:38","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("138","12","2019-06-04 16:58:44","El usuario 127001 ha creado un comentario","127001");
INSERT INTO Logs VALUES("139","0","2019-06-04 16:58:52","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("140","2","2019-06-04 16:19:24","Se ha modificado el usuario admin","1");
INSERT INTO Logs VALUES("141","2","2019-06-04 16:19:46","Se ha modificado el usuario Pablo","1");
INSERT INTO Logs VALUES("142","2","2019-06-04 16:20:12","Se ha modificado el usuario Iluminada","1");
INSERT INTO Logs VALUES("143","2","2019-06-04 16:20:55","Se ha modificado el usuario Iluminada","1");
INSERT INTO Logs VALUES("144","2","2019-06-04 16:21:29","Se ha modificado el usuario profesor","1");
INSERT INTO Logs VALUES("145","1","2019-06-04 16:21:51","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("146","0","2019-06-04 16:21:56","Usuario profesor ha iniciado sesión","8");
INSERT INTO Logs VALUES("147","1","2019-06-04 16:21:57","Usuario profesor ha cerrado sesión","8");
INSERT INTO Logs VALUES("148","0","2019-06-04 16:22:00","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("149","1","2019-06-04 16:25:28","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("150","4","2019-06-04 16:25:34","Error al iniciar sesión con usuario Iluminada","127001");
INSERT INTO Logs VALUES("151","4","2019-06-04 16:25:44","Error al iniciar sesión con usuario Iluminada","127001");
INSERT INTO Logs VALUES("152","0","2019-06-04 16:25:47","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("153","2","2019-06-04 16:25:58","Se ha modificado el usuario Iluminada","1");
INSERT INTO Logs VALUES("154","1","2019-06-04 16:26:00","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("155","0","2019-06-04 16:26:05","Usuario Iluminada ha iniciado sesión","7");
INSERT INTO Logs VALUES("156","2","2019-06-04 16:27:14","Se ha modificado el usuario Iluminada","1");
INSERT INTO Logs VALUES("157","9","2019-06-04 16:30:15","Usuario Iluminada ha creado la incidencia Cambio de ponderación de practicas con id 13","7");
INSERT INTO Logs VALUES("158","10","2019-06-04 16:31:43","Usuario Iluminada ha subido una imagen (Views/img/incidencias/inyustisia.gif) a la incidencia 13","7");
INSERT INTO Logs VALUES("159","1","2019-06-04 16:31:55","Usuario Iluminada ha cerrado sesión","7");
INSERT INTO Logs VALUES("160","0","2019-06-04 16:32:17","Usuario Pablo ha iniciado sesión","4");
INSERT INTO Logs VALUES("161","12","2019-06-04 16:33:04","El usuario 4 ha creado un comentario","4");
INSERT INTO Logs VALUES("162","12","2019-06-04 16:33:12","El usuario 4 ha borrado un comentario","4");
INSERT INTO Logs VALUES("163","12","2019-06-04 16:33:17","El usuario 4 ha creado un comentario","4");
INSERT INTO Logs VALUES("164","1","2019-06-04 16:33:22","Usuario Pablo ha cerrado sesión","4");
INSERT INTO Logs VALUES("165","0","2019-06-04 16:33:30","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("166","2","2019-06-04 16:33:44","Se ha modificado el usuario Victor","1");
INSERT INTO Logs VALUES("167","1","2019-06-04 16:34:00","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("168","0","2019-06-04 16:34:06","Usuario Victor ha iniciado sesión","5");
INSERT INTO Logs VALUES("169","12","2019-06-04 16:34:22","El usuario 5 ha creado un comentario","5");
INSERT INTO Logs VALUES("170","1","2019-06-04 16:34:40","Usuario Victor ha cerrado sesión","5");
INSERT INTO Logs VALUES("171","0","2019-06-04 16:34:49","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("172","7","2019-06-04 16:35:08","Usuario admin ha actualizado la incidencia 13","1");
INSERT INTO Logs VALUES("173","11","2019-06-04 16:35:21","Usuario admin ha votado positivo a la incidencia 13","1");
INSERT INTO Logs VALUES("174","1","2019-06-04 16:35:23","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("175","0","2019-06-04 16:35:31","Usuario Victor ha iniciado sesión","5");
INSERT INTO Logs VALUES("176","11","2019-06-04 16:35:34","Usuario Victor ha votado positivo a la incidencia 13","5");
INSERT INTO Logs VALUES("177","12","2019-06-04 16:35:37","Usuario Victor ha votado negativo a la incidencia 12","5");
INSERT INTO Logs VALUES("178","11","2019-06-04 16:35:41","Usuario Victor ha votado positivo a la incidencia 11","5");
INSERT INTO Logs VALUES("179","11","2019-06-04 16:35:44","Usuario Victor ha votado positivo a la incidencia 11","5");
INSERT INTO Logs VALUES("180","12","2019-06-04 16:35:48","Usuario Victor ha votado negativo a la incidencia 11","5");
INSERT INTO Logs VALUES("181","11","2019-06-04 16:35:52","Usuario Victor ha votado positivo a la incidencia 11","5");
INSERT INTO Logs VALUES("182","11","2019-06-04 16:35:55","Usuario Victor ha votado positivo a la incidencia 10","5");
INSERT INTO Logs VALUES("183","12","2019-06-04 16:35:59","Usuario Victor ha votado negativo a la incidencia 9","5");
INSERT INTO Logs VALUES("184","11","2019-06-04 16:36:04","Usuario Victor ha votado positivo a la incidencia 3","5");
INSERT INTO Logs VALUES("185","12","2019-06-04 16:36:08","Usuario Victor ha votado negativo a la incidencia 1","5");
INSERT INTO Logs VALUES("186","1","2019-06-04 16:36:10","Usuario Victor ha cerrado sesión","5");
INSERT INTO Logs VALUES("187","0","2019-06-04 16:36:37","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("188","1","2019-06-04 16:43:45","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("189","0","2019-06-04 16:43:50","Usuario Pablo ha iniciado sesión","4");
INSERT INTO Logs VALUES("190","11","2019-06-04 16:43:53","Usuario Pablo ha votado positivo a la incidencia 13","4");
INSERT INTO Logs VALUES("191","1","2019-06-04 16:44:29","Usuario Pablo ha cerrado sesión","4");
INSERT INTO Logs VALUES("192","0","2019-06-04 16:44:32","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("193","2","2019-06-04 16:46:19","Se ha modificado el usuario Iluminada","1");
INSERT INTO Logs VALUES("194","1","2019-06-04 16:47:05","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("195","0","2019-06-04 16:47:54","Usuario admin ha iniciado sesión","1");
INSERT INTO Logs VALUES("196","7","2019-06-04 16:49:42","Usuario admin ha actualizado la incidencia 13","1");
INSERT INTO Logs VALUES("197","8","2019-06-04 16:49:56","Usuario admin ha cambiado el estado de la incidencia 13 a Irresoluble","1");
INSERT INTO Logs VALUES("198","11","2019-06-04 16:50:09","Usuario admin ha votado positivo a la incidencia 13","1");
INSERT INTO Logs VALUES("199","1","2019-06-04 16:50:15","Usuario admin ha cerrado sesión","1");
INSERT INTO Logs VALUES("200","0","2019-06-04 16:51:54","Usuario admin ha iniciado sesión","1");


DROP TABLE Imagenes;

CREATE TABLE `Imagenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_incidencia` int(11) NOT NULL,
  `imagen` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Imagenes_fk0` (`id_incidencia`),
  CONSTRAINT `Imagenes_fk0` FOREIGN KEY (`id_incidencia`) REFERENCES `Incidencias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO Imagenes VALUES("1","1","Views/img/incidencias/apagones.jpg");
INSERT INTO Imagenes VALUES("2","12","Views/img/incidencias/obras.jpg");
INSERT INTO Imagenes VALUES("3","11","Views/img/incidencias/gotera.JPG");
INSERT INTO Imagenes VALUES("4","9","Views/img/incidencias/mierdaperro.jpg");
INSERT INTO Imagenes VALUES("5","3","Views/img/incidencias/huelga.jpg");
INSERT INTO Imagenes VALUES("6","8","Views/img/incidencias/arbolcaido.jpg");
INSERT INTO Imagenes VALUES("7","5","Views/img/incidencias/grafiti.jpg");
INSERT INTO Imagenes VALUES("8","2","Views/img/incidencias/retrovisorroto.jpg");
INSERT INTO Imagenes VALUES("9","4","Views/img/incidencias/ladridoperro.png");
INSERT INTO Imagenes VALUES("10","13","Views/img/incidencias/inyustisia.gif");


DROP TABLE Comentarios;

CREATE TABLE `Comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `texto` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_incidencia` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Comentarios_fk0` (`id_incidencia`),
  CONSTRAINT `Comentarios_fk0` FOREIGN KEY (`id_incidencia`) REFERENCES `Incidencias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO Comentarios VALUES("1","2019-06-04 11:54:56","","2","1");
INSERT INTO Comentarios VALUES("2","2019-06-04 11:55:24","son obras, no hay nada que hacer","12","1");
INSERT INTO Comentarios VALUES("3","2019-06-04 11:55:31","a oscuras se vive mejor!","1","1");
INSERT INTO Comentarios VALUES("4","2019-06-04 11:57:05","ya pero, son muy molestos!","12","6");
INSERT INTO Comentarios VALUES("5","2019-06-04 11:57:20","uf, espero que no haya pasado ninguna desgracia! Llama a la policía","11","6");
INSERT INTO Comentarios VALUES("6","2019-06-04 11:57:37","Habrá que llamar a la compañía eléctrica. Estas cosas pasan...","1","6");
INSERT INTO Comentarios VALUES("7","2019-06-04 11:58:15","Hay que manifestarse! Muy bien hecho","3","6");
INSERT INTO Comentarios VALUES("8","2019-06-04 11:58:26","Que gente de verdad, habría que hacerselo a ellos en la cabeza!","2","6");
INSERT INTO Comentarios VALUES("9","2019-06-04 12:07:02","no nos podemos quejar de todo Rosa...","12","7");
INSERT INTO Comentarios VALUES("10","2019-06-04 12:07:35","A la luz de las velas es más romántico","1","7");
INSERT INTO Comentarios VALUES("11","2019-06-04 12:07:47","se habrán ido de vacaciones","11","7");
INSERT INTO Comentarios VALUES("12","2019-06-04 12:08:01","qué asco!","9","7");
INSERT INTO Comentarios VALUES("13","2019-06-04 12:08:20","Que vergüenza!","2","7");
INSERT INTO Comentarios VALUES("14","2019-06-04 12:08:58","Totalmente de acuerdo!","3","7");
INSERT INTO Comentarios VALUES("15","2019-06-04 12:09:20","Yo tengo perro y recojo todas las cacas","9","7");
INSERT INTO Comentarios VALUES("16","2019-06-04 12:10:53","Qué peligro! Ya no se puede ni ir por la calle","8","7");
INSERT INTO Comentarios VALUES("17","2019-06-04 16:58:44","A mi también me pasa","12","127001");
INSERT INTO Comentarios VALUES("19","2019-06-04 16:33:17","Que calvario","13","4");
INSERT INTO Comentarios VALUES("20","2019-06-04 16:34:22","Menos quejarse y mas programar","13","5");


SET FOREIGN_KEY_CHECKS=1;
