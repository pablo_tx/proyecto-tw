<?php

/**
 * *****************************************************************************************************************
 * Usuario sin identificar:                                                                                        *
 * *****************************************************************************************************************
 * Boton central para Ver Incidencias. Panel lateral derecha con login y los que mas aniaden y los que mas opinan.
 * Pagina de inicio un titulo con una descripcion de la web.
 * 
 * Las incidencias, tienen Lugar,Fecha, Creeador, Palabras Clave, Estado y Valoraciones.
 * Tambi'en tienen comentarios, se puede aniadir comentario. Dos botones, uno negativo y otro positivo.
 * Si le das al mas aniades opinon positiva.
 * Al menos opinion negativa
 * IMagen de globo de texto para aniadir comentario.
 * 
 * Tambien tiene Busqueda de incidencias. Buysqueda es un panel con varios checkbox. Se puede buscar por
 * Lugar, TExto de busqueda,
 * Estado: pendiente,comprobada terminada irresoluble, resuelta.
 * 
 * ORdenar por antiguedad, numero de positivos, numero de positivos netos 
 * 
 * 
 * *****************************************************************************************************************
 * Usuario colaborador                                                                                             *
 * *****************************************************************************************************************
 * Una vez hace log in un colaborador. Tiene un panel con 2 items m'as.
 * Ver indicencias, Nueva Incidencia, Mis incidencias.
 * En el panel de log in, ahora aparece una foto con la cara del usuario.  Y dos botones abajo Logout y Editar.
 * 
 * Esto es un panel con una imagen que se puede subir
 * Nombre,Apellidos,Email,Clave,Direccion,RTElefono,Rol,Estado y Boton para modificar usiario
 * Rol y EStado son desplegables. select
 * 
 * Pestania en NAV Nueva incidencia.
 * Esto tiene. Titulo, Descripcion, Lugar, y paalbras clave y tambien una fotograf'ia. LAs fotografias tendran un boton para borrarlas.
 * La incidencia tambien tiene 5 checkbox, Pendiente, Comprobada, Tramitada, Irresoluble, Resuelta.
 * 
 * Al ser usuario, 
 * 
 * 
 * 
 * Para crear una incidencia, cuando se muestran las incidencias tienes nuevos botones. Editar y Borrar la incidencia.
 * 
 * *****************************************************************************************************************
 * Usuario administrador                                                                                           *
 * *****************************************************************************************************************
 * Si tienes una cuenta tipo administrador.
 * Se aniaden nuevas funcioonalidaddes al nav. Como Ver log y Gestion de BDD y Gestion de USUarios.
 * En el panel de Ver incidencias, tienes un nuevo boton al lado de cada comentario para poder borrar los comentarios.
 * Puedes borrar tanto comentarios como incidencias.
 * 
 * El usuario administrador tambiendebe encargarse de la gestion de usuarios. En esta pestania sale una lista de todos los usuarios.
 * La lista esta encabezada por dos items. Listado y aniadir nuevo. SI le das a listado se listan todos los usuarios y ANiadir nuevo 
 * salta un formulario para aniadir usuario.
 * 
 * Formulario Aniadir USuario:
 * Foto, Nombre, Apellidos, Email, Clave, Direccion, Telefono, Rol, Estado, botonCrearUSuario. Al aniadir nuevo usuario,
 * ya podra hacer log in en nuestro sistema. Con las credenciales oportunas./
 * 
 * LA parte del LLOG. Ver Log en el nav. 
 * Deben salir todos los logs del sistema. Cada vez que hacemos algo llamamos al controlador de logs y metemos linea en BD.
 * 
 * Gestion de la Base de datos. En la pestania NAV.
 * 
 * Tenemos tres opciones.
 * Descargar copia de seguridad. HAce un mysqldump y se descarga el fichero.
 * Restaurar copia de seguridad. Se elige un fichero .sql y se realiza un mysql database < .sql para restaurar la BDD.
 * Borrar BDD, si se borra la BDD, se borra pero siguen habiendo usuarios. 
 * 
 * 
 * 
 * 
 * 
 */

require_once(__DIR__ . "/Core/request.php");
require_once(__DIR__ . "/Core/router.php");
require_once(__DIR__ . "/Core/config.php");

$request = new Request($_SERVER['REQUEST_URI']);
$router = new Router($request);
$router->dispatch();
