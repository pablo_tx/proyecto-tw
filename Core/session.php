<?php
class Session{
    public static function createSession(){
        session_start();
    }

    public static function getSessionVar($key){
        if(isset($_SESSION[$key]))
            return $_SESSION[$key];
        else
            return null;
    }

    public static function setSessionVar($key, $content){
        $_SESSION[$key] = $content;
    }

    public static function closeSession(){
        // Iniciar sesión si no lo está
        if (session_status() == PHP_SESSION_NONE)
            session_start();

        // Borrar variables de sesión
        session_unset();

        // Obtener parámetros de cookie de sesión
        $param = session_get_cookie_params();

        // Borrar cookie de sesión
        setcookie(session_name(), $_COOKIE[session_name()], time()-2592000, $param['path'], $param['domain'], $param['secure'], $param['httponly']);

        // Destruir sesión
        session_destroy();
    }

}

?>