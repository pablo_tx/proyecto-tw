<?php


// Incluir controllers
spl_autoload_register(function($className) {
    if(file_exists('Controllers/' . strtolower($className). '.php')){
        include_once 'Controllers/' . strtolower($className). '.php';
    }
});

class Router
{

    private $requestBase;
    private $requestParams = [];
    private $selectedController;
    private $routeList = [
        ''                    =>  ["page", "index"],
        'index'               =>  ["page", "index"],
        'incidencias'         =>  ["page", "index"],
        'incidencias/nueva'   =>  ["page", "nuevaincidencia"],
        'incidencias/create'  =>  ["incidencia", "create"],
        'incidencias/addimage'=>  ["incidencia", "addimage"],
        'incidencias/update'  =>  ["incidencia", "update"],
        'incidencias/update_estado'  =>  ["incidencia", "update_estado"],
        'incidencias/delete'  =>  ["incidencia", "delete"],
        'incidencias/votarmas'  =>  ["incidencia", "votarmas"],
        'incidencias/votarmenos'  =>  ["incidencia", "votarmenos"],
        'incidencias/comentarincidencia'  =>  ["incidencia", "comentarincidencia"],
        'incidencias/borrarcomentario'  =>  ["incidencia", "borrarcomentario"],
        'incidencias/ver'     =>  ["page", "incidencia"],
        'incidencias/editar'  =>  ["page", "editarincidencia"],
        'incidencias/usuario' =>  ["page", "usuarioincidencias"],
        'logs'                =>  ["page", "logs"],
        'gestion/usuario/create'    =>  ["usuario", "create"],
        'gestion/usuario/borrar'    =>  ["usuario", "borrar"],
        'gestion/usuario/update'    =>  ["usuario", "update"],
        'usuario/editar'    =>  ["page", "editarusuario"],
        'gestion/bbdd'        =>  ["page", "gestionbbdd"],
        'gestion/bbdd/descargar'        =>  ["backup", "download"],
        'gestion/bbdd/restaurar'        =>  ["backup", "restore"],
        'gestion/bbdd/borrar'        =>  ["backup", "delete"],
        'gestion/usuarios'        =>  ["page", "gestionusuarios"],
        'pdf'        =>  ["page", "pdf"],
        'login'               =>  ["login", "login"],
        'logout'              =>  ["login", "logout"],
        '404'                 =>  ["page", "404"],
    ];


    public function __construct($request){
        // Obtener ruta base (ruta de lista de rutas)
        $this->requestBase = $this->findBaseRoute($request->getContent());
        // Obtener parametros (resto de la url)
        $this->requestParams = $this->requestBase != '404' ? ltrim(rtrim(substr($request->getContent(), strlen($this->requestBase)),"/"),"/") : '';
        // Obtener controlador asociado a la ruta
        $this->selectedController = $this->getRouteController();
    }

    public function findBaseRoute($request)
    {
        // Eliminar query string
        $removeQueryStrings = strtok($request, '?');
        // Separar por /
        $split = explode('/', $removeQueryStrings);

        // Limitar numero de parametros a buscar
        count($split) > 4 ? $x = 4 : $x = count($split);

        for ($x; $x > 0; $x--) {
            // Crear clave de mayor a menor numero de parametros - [catalogo/nuevo/xxx] | [catalogo/nuevo] | [catalogo]
            $key = '';
            for ($i = 0; $i < $x; $i++) {
                $key .= $split[$i].'/';
            }
            $key = rtrim($key,"/");

            // Comprobar si existe la clave actual en la lista de rutas
            if (array_key_exists($key, $this->routeList)) {
                return $key;
            }
        }
        // Si no se ha encontrado, devolver 404
        return '404';
    }

    public function getRouteController()
    {
        $selected = '';
        // Volver a comprobar si la ruta está en la lista
        if (array_key_exists($this->requestBase, $this->routeList)) {
            $selected = $this->routeList[$this->requestBase];
        }

        // Mostrar 404 si no encuentra la ruta
        if(is_null($selected)) {
            $selected = $this->routeList['404'];
        }

        // TODO: quitar comprobaciones innecesarias?
        //return $selected = $this->routeList[$this->requestBase];
        return $selected;
    }

    public function getRoutes()
    {
        return $this->routeList;
    }

    public function getParams()
    {
        return $this->requestParams;
    }

    // Lanza la petición al controlador
    public function dispatch()
    {
        $controller = $this->selectedController[0];
        $controllerParam = $this->selectedController[1];

        if (class_exists(ucfirst($controller))) {
            $controller_object = new $controller();
            if (Config::DEBUG):
                echo "Router info<br>";
                echo "controller: $controller <br>";
                echo "controllerParam: $controllerParam <br>";
                echo "requestParams: $this->requestParams <br>";
            endif;
            $controller_object->run($controllerParam, $this->requestParams);
        } else {
            throw new Exception("Controller class $controller not found");
        }
    }
}