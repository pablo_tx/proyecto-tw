<?php


/**
 * Class Database. Define las funciones principales de conexión y desconexión a la base de datos.
 */
class Database{

    /**
     * @var Instancia de la base de datos.
     */
    private static $instance;

    /**
     * Database constructor.
     */
    private function __construct(){}

    /**
     * Función para conectarse con la base de datos definida en el fichero de configuración
     * @return PDO
     */
    private static function connect(){
        $dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
        $instance = new PDO($dsn, Config::DB_USER, Config::DB_PASS);

        // Lanzar excepciones
        $instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $instance;
    }

    /**
     * Función que devuelve la instancia de la BD en cuestión.
     * @return PDO
     */
    public static function getInstance(){
        if(!self::$instance){
            self::$instance = self::connect();
        }
        return self::$instance;
    }

}

?>