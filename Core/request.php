<?php

require_once "session.php";

class Request
{
    private $base;


    public function __construct($QUERY_STRING){
        Session::createSession();
        $this->base = $this->clean($QUERY_STRING);
        if (Config::DEBUG):
            echo "Request info<br>";
            echo "Original: $QUERY_STRING <br>";
            echo "Clean: $this->base <br>";
        endif;
    }

    public function getContent(){
        return $this->base;
    }

    private function clean($url){
        $cleanurl = substr($url, strlen(Config::BASE_URL));

        $cleanurl = strip_tags($cleanurl);

        return $cleanurl;
    }
}