<?php

final class Config
{
    /**
     ** Activar depuración
     */
    const DEBUG = false;

    /**
     ** URL Base de la aplicación
     */
    const BASE_URL = "/proyecto/";
    //const BASE_URL = "/~pablopt1819/proyecto/";
    //const BASE_URL = "/~victormoreno1819/proyecto/";

    /**
     ** Contraseña de la base de datos
     */
    const DB_NAME = "proyecto";

    /**
     ** Host de la base de datos
     */
    const DB_HOST = "localhost";

    /**
     ** Contraseña de la base de datos
     */
    const DB_PORT = "3306";

    /**
     ** Usuario de la base de datos
     */
    const DB_USER = "admin";

    /**
     ** Contraseña de la base de datos
     */
    const DB_PASS = "admin";

}

?>