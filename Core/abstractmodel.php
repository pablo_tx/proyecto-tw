<?php

require_once "database.php";

abstract class AbstractModel{
    protected $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function tableExists($table){
        $statement = $this->db->prepare("SELECT COUNT(*) AS NUM 
              FROM information_schema.tables 
              WHERE table_schema=:currentSchema
              AND table_name=:table");

        $statement->execute(array(':currentSchema' => $this->getCurrentSchema(), ':table' =>$table));

        $result = $statement->fetch();

        return $result['NUM'] != 0 ? true : false;
    }

    private function getCurrentSchema(){
        return Config::DB_NAME;
    }
}

?>