<?php


interface ControllerInterface
{
    // Ejecuta la acción del controlador
    public function run($controllerParam, $requestParams);
}